package org.venity.vgit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;
import org.venity.vgit.backend.configuratons.filters.VGitUrlFilter;
import org.venity.vgit.backend.git.servlets.VGitServlet;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(VGitConfigurationProperties.class)
public class VGitApplication {
    public static void main(String[] args) {
        SpringApplication.run(VGitApplication.class, args);
    }

    @Bean
    public ServletRegistrationBean<VGitServlet> httpGitRepositories(VGitServlet servlet) {
        return new ServletRegistrationBean<>(servlet, VGitServlet.SERVLET_URL_MAPPING);
    }

    @Bean
    public FilterRegistrationBean<VGitUrlFilter> gitUrlFilterFilterRegistrationBean() {
        return new FilterRegistrationBean<>(new VGitUrlFilter());
    }
}
