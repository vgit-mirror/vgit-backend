package org.venity.vgit.frontend;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.venity.vgit.backend.api.controllers.user.UserRegisterController;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class RegistrationController extends AbstractController {
    private final UserRegisterController userRegisterController;

    public RegistrationController(UserDatabase userDatabase, UserRegisterController userRegisterController, VGitConfigurationProperties configurationProperties) {
        super(userDatabase, configurationProperties);
        this.userRegisterController = userRegisterController;
    }

    @GetMapping("/registration")
    public String login(HttpServletRequest request, Map<String, Object> context) {
        try {
            requireAuth(request);
            return "redirect:/";
        } catch (AuthenticationException e) {
            return "registration";
        }
    }

    @PostMapping("/registration")
    public String postRegistration(HttpServletRequest request, Map<String, Object> context, String username, String fullName, String email, String password) {
        try {
            userRegisterController.reg(username, fullName, email, password);
        } catch (APIMethodException e) {
            context.put("error", e.getMessage());
            context.put("username", username);
            context.put("fullName", fullName);
            context.put("email", email);

            return "registration";
        }

        return "redirect:/login";
    }
}
