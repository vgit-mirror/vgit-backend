package org.venity.vgit.frontend;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
public class UserProfileController extends AbstractController {
    private final UserDatabase userDatabase;
    private final RepositoryDatabase repositoryDatabase;

    public UserProfileController(UserDatabase userDatabase, RepositoryDatabase repositoryDatabase, VGitConfigurationProperties configurationProperties) {
        super(userDatabase, configurationProperties);

        this.userDatabase = userDatabase;
        this.repositoryDatabase = repositoryDatabase;
    }

    @GetMapping("/{user}")
    public String profile(HttpServletRequest request, @PathVariable String user, Map<String, Object> context) {
        UserPrototype userP = null;
        try {
            userP = requireAuth(request);
            context.put("user", userP);
        } catch (AuthenticationException ignored) {
            context.put("user", null);
        }
        UserPrototype userPrototype = userDatabase.findByLogin(user);

        if (userPrototype == null)
            return "error";

        List<RepositoryPrototype> repositoryPrototypes = new ArrayList<>();

        for (Long repo : userPrototype.getRepositories()) {
            Optional<RepositoryPrototype> repositoryPrototypeOptional = repositoryDatabase.findById(repo);
            if (repositoryPrototypeOptional.isEmpty())
                continue;

            RepositoryPrototype repositoryPrototype = repositoryPrototypeOptional.get();

            if (repositoryPrototype.isConfidential()) {
                if (userP != null)
                    if (repositoryPrototype.getOwners().contains(userP.getId()) || repositoryPrototype.getMembers().contains(userP.getId()))
                        repositoryPrototypes.add(repositoryPrototype);
            } else repositoryPrototypes.add(repositoryPrototype);
        }

        context.put("profile", userPrototype);
        context.put("repositories", repositoryPrototypes);
        return "profile";
    }
}
