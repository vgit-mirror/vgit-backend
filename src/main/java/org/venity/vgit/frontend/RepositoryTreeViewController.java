package org.venity.vgit.frontend;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.jgit.api.Git;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.venity.vgit.backend.api.controllers.repository.GitRepositoryRawController;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.prototypes.CommitPrototype;
import org.venity.vgit.backend.api.prototypes.FilePrototype;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.Collection;
import java.util.Map;

@Controller
public class RepositoryTreeViewController extends AbstractController {
    private final RepositoryDatabase repositoryDatabase;
    private final VGitConfigurationProperties configurationProperties;
    private final GitRepositoryRawController gitRepositoryRawController;

    public RepositoryTreeViewController(UserDatabase userDatabase, RepositoryDatabase repositoryDatabase, VGitConfigurationProperties configurationProperties, GitRepositoryRawController gitRepositoryRawController) {
        super(userDatabase, configurationProperties);
        this.repositoryDatabase = repositoryDatabase;
        this.configurationProperties = configurationProperties;
        this.gitRepositoryRawController = gitRepositoryRawController;
    }

    @GetMapping("/{namespace}/{repository}/tree/{commit}/**")
    public String treeView(HttpServletRequest request, Map<String, Object> context,
                           @PathVariable String namespace, @PathVariable String repository, @PathVariable String commit) {
        RepositoryPrototype repositoryPrototype
                = repositoryDatabase.findByPath(namespace + "/" + repository);

        if (repositoryPrototype == null)
            return "error";

        UserPrototype userPrototype = null;
        try {
            userPrototype = requireAuth(request);
            context.put("user", userPrototype);
        } catch (AuthenticationException ignored) {
            context.put("user", null);
        }

        if (repositoryPrototype.isConfidential())
            if (userPrototype == null)
                return "error";
            else if (!(repositoryPrototype.getOwners().contains(userPrototype.getId())
                    || repositoryPrototype.getMembers().contains(userPrototype.getId())))
                return "error";

        context.put("repository", repositoryPrototype);
        context.put("namespace", namespace);
        context.put("urls", repositoryURLS(userPrototype, repositoryPrototype, request));

        String path = request.getRequestURI().substring((namespace + "/" + repository + "/tree/" + commit + "/").length());
        context.put("path", path);
        context.put("commitName", commit);

        try {
            Collection<FilePrototype> collection =  gitRepositoryRawController.getTree(repositoryPrototype.getPath(), path, getToken(request), commit);
            CommitPrototype commitPrototype = null;

            for (FilePrototype prototype : collection) {
                if (commitPrototype == null) {
                    commitPrototype = prototype.commit;
                    continue;
                }

                if (commitPrototype.time <= prototype.commit.time)
                    commitPrototype = prototype.commit;
            }

            context.put("tree", collection);
            context.put("lastCommit", commitPrototype);
        } catch (Exception e) {
            context.put("error", e.getMessage());
            return "error";
        }

        return "repository/treeView";
    }

    @GetMapping("/{namespace}/{repository}/file/{commit}/**")
    public String fileView(HttpServletRequest request, Map<String, Object> context,
                           @PathVariable String namespace, @PathVariable String repository, @PathVariable String commit) {
        RepositoryPrototype repositoryPrototype
                = repositoryDatabase.findByPath(namespace + "/" + repository);

        if (repositoryPrototype == null)
            return "error";

        UserPrototype userPrototype = null;
        try {
            userPrototype = requireAuth(request);
            context.put("user", userPrototype);
        } catch (AuthenticationException ignored) {
            context.put("user", null);
        }

        Git git = null;
        try {
            git = Git.open(new File(configurationProperties.getRepositoryRoot(), repositoryPrototype.getPath()));
        } catch (Exception e) {
            context.put("error", e.getMessage());
            return "error";
        }

        if (repositoryPrototype.isConfidential())
            if (userPrototype == null)
                return "error";
            else if (!(repositoryPrototype.getOwners().contains(userPrototype.getId())
                    || repositoryPrototype.getMembers().contains(userPrototype.getId())))
                return "error";

        context.put("repository", repositoryPrototype);
        context.put("namespace", namespace);

        String path = request.getRequestURI().substring((namespace + "/" + repository + "/tree/" + commit + "/").length());
        context.put("path", path);
        context.put("commitName", commit);

        try {
            context.put("lastCommit", git.log().addPath(path).setMaxCount(1).call().iterator().next());
        } catch (Exception e) {
            context.put("error", e.getMessage());
            return "error";
        }

        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            git.getRepository().open(gitRepositoryRawController
                    .getSingletonTreeWalkByPath(repositoryPrototype.getPath(), path, getToken(request), commit)
                    .getObjectId(0))
                    .copyTo(outputStream);

            context.put("fileContent", outputStream.toString());
            context.put("fileExtension", FilenameUtils.getExtension(path));
        } catch (Exception e) {
            context.put("error", e.getMessage());
            return "error";
        }

        return "repository/fileView";
    }
}
