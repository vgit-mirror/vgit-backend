package org.venity.vgit.frontend;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class IndexController extends AbstractController implements ErrorController {

    public IndexController(UserDatabase userDatabase, VGitConfigurationProperties configurationProperties) {
        super(userDatabase, configurationProperties);
    }

    @GetMapping("/")
    public String index(HttpServletRequest request, Map<String, Object> context) {
        addUser(request, context);

        if (context.get("user") != null)
            return "main";

        return "landing";
    }

    @GetMapping("/error")
    public String error(HttpServletRequest request, Map<String, Object> context) {
        addUser(request, context);
        context.put("code", 404);
        return "error";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
