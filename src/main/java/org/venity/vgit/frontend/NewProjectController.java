package org.venity.vgit.frontend;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.venity.vgit.backend.api.controllers.repository.GitRepositoryController;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class NewProjectController extends AbstractController {
    private final GitRepositoryController gitRepositoryController;

    public NewProjectController(UserDatabase userDatabase, GitRepositoryController gitRepositoryController, VGitConfigurationProperties configurationProperties) {
        super(userDatabase, configurationProperties);
        this.gitRepositoryController = gitRepositoryController;
    }

    @GetMapping("/new/project")
    public String newProject(HttpServletRequest request, Map<String, Object> context) {
        try {
            context.put("user", requireAuth(request));
            return "new/project";
        } catch (AuthenticationException e) {
            return "redirect:/login";
        }
    }

    @PostMapping("/new/project")
    public String postNewProject(HttpServletRequest request, Map<String, Object> context,
                                 String name, String path, String description, String confidential) {
        UserPrototype userPrototype = null;
        try {
            userPrototype = requireAuth(request);
        } catch (AuthenticationException e) {
            return "redirect:/login";
        }

        try {
            RepositoryPrototype repositoryPrototype
                    = gitRepositoryController.makeRepo(getToken(request), name, path, description, confidential);
            context.put("repository", repositoryPrototype);
            return "redirect:/" + repositoryPrototype.getPath();
        } catch (APIMethodException e) {
            context.put("user", userPrototype);
            context.put("error", e.getMessage());
            context.put("name", name);
            context.put("path", path);
            context.put("description", description);

            return "new/project";
        }
    }
}
