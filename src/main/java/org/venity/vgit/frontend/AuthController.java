package org.venity.vgit.frontend;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.venity.vgit.backend.api.controllers.user.UserAuthController;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import javax.naming.AuthenticationException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Controller
@Slf4j
public class AuthController extends AbstractController {
    private final UserAuthController userAuthController;

    public AuthController(UserDatabase userDatabase, UserAuthController userAuthController, VGitConfigurationProperties configurationProperties) {
        super(userDatabase, configurationProperties);
        this.userAuthController = userAuthController;
    }

    @GetMapping("/login")
    public String login(HttpServletRequest request, Map<String, Object> context) {
        try {
            requireAuth(request);
            return "redirect:/";
        } catch (AuthenticationException e) {
            return "login";
        }
    }

    @PostMapping("/login")
    public String postLogin(HttpServletResponse response, Map<String, Object> context, String username, String password) {
        try {
            Cookie cookie = new Cookie("TOKEN", userAuthController.login(username, password).get("token"));
            cookie.setPath("/");
            response.addCookie(cookie);
        } catch (APIMethodException e) {
            context.put("error", e.getMessage());
            context.put("username", username);
            return "login";
        }

        return "redirect:/";
    }

    @GetMapping("/auth/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws IOException {
        try {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals("TOKEN") && cookie.getValue() != null) {
                    Cookie newToken = new Cookie("TOKEN", null);
                    newToken.setPath("/");
                    response.addCookie(newToken);
                    userAuthController.logout(cookie.getValue());
                    return "redirect:/login";
                }
            }
        } catch (Exception ignored) {

        }

        return "redirect:/login";
    }
}
