package org.venity.vgit.frontend;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import javax.naming.AuthenticationException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public abstract class AbstractController {
    private final UserDatabase userDatabase;
    private final VGitConfigurationProperties configurationProperties;

    public AbstractController(UserDatabase userDatabase, VGitConfigurationProperties configurationProperties) {
        this.userDatabase = userDatabase;
        this.configurationProperties = configurationProperties;
    }

    protected UserPrototype requireAuth(HttpServletRequest request) throws AuthenticationException {
        try {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals("TOKEN") && !cookie.getValue().isEmpty()) {
                    return userDatabase.findByTokensIn(cookie.getValue());
                }
            }
        } catch (NullPointerException e) {
            throw new AuthenticationException();
        }

        throw new AuthenticationException();
    }

    protected String getToken(HttpServletRequest request) {
        try {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals("TOKEN") && !cookie.getValue().isEmpty()) {
                    return cookie.getValue();
                }
            }
        } catch (NullPointerException e) {
            return "";
        }

        return "";
    }

    protected void addUser(HttpServletRequest request, Map<String, Object> context) {
        try {
            context.put("user", requireAuth(request));
        } catch (AuthenticationException e) {

        }
    }

    protected GitRepositoryURLS repositoryURLS(UserPrototype userPrototype, RepositoryPrototype repositoryPrototype, HttpServletRequest request) {
        GitRepositoryURLS urls = new GitRepositoryURLS();

        StringBuilder httpUrlBuilder = new StringBuilder();
        httpUrlBuilder.append(request.getScheme());
        httpUrlBuilder.append("://");
        httpUrlBuilder.append(request.getServerName());

        if (request.getServerPort() != 80 && request.getServerPort() != 433) {
            httpUrlBuilder.append(":");
            httpUrlBuilder.append(request.getServerPort());
        }

        httpUrlBuilder.append("/");
        httpUrlBuilder.append(repositoryPrototype.getPath());
        httpUrlBuilder.append(".git");

        urls.setHttp(httpUrlBuilder.toString());

        if (configurationProperties.getSsh().getEnabled()) {
            StringBuilder sshUrlBuilder = new StringBuilder();
            sshUrlBuilder.append("ssh://");

            if (userPrototype != null) {
                sshUrlBuilder.append(userPrototype.getLogin());
                sshUrlBuilder.append("@");
            }

            sshUrlBuilder.append(request.getServerName());

            if (configurationProperties.getSsh().getPort() != 22) {
                sshUrlBuilder.append(":");
                sshUrlBuilder.append(configurationProperties.getSsh().getPort());
            }

            sshUrlBuilder.append("/");
            sshUrlBuilder.append(repositoryPrototype.getPath());
            sshUrlBuilder.append(".git");

            urls.setSsh(sshUrlBuilder.toString());
        }

        return urls;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GitRepositoryURLS {
        private String http;
        private String ssh;
    }
}
