package org.venity.vgit.frontend;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.venity.vgit.backend.api.controllers.repository.GitRepositoryRawController;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.prototypes.CommitPrototype;
import org.venity.vgit.backend.api.prototypes.FilePrototype;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Map;

@Controller
public class RepositoryViewController extends AbstractController {
    private final RepositoryDatabase repositoryDatabase;
    private final GitRepositoryRawController gitRepositoryRawController;

    public RepositoryViewController(UserDatabase userDatabase, RepositoryDatabase repositoryDatabase, VGitConfigurationProperties configurationProperties, GitRepositoryRawController gitRepositoryRawController) {
        super(userDatabase, configurationProperties);
        this.repositoryDatabase = repositoryDatabase;
        this.gitRepositoryRawController = gitRepositoryRawController;
    }

    @GetMapping("/{namespace}/{repository}")
    public String repoView(HttpServletRequest request, Map<String, Object> context,
                           @PathVariable String namespace, @PathVariable String repository) {
        RepositoryPrototype repositoryPrototype
                = repositoryDatabase.findByPath(namespace + "/" + repository);

        if (repositoryPrototype == null)
            return "error";

        UserPrototype userPrototype = null;
        try {
            userPrototype = requireAuth(request);
            context.put("user", userPrototype);
        } catch (AuthenticationException ignored) {
            context.put("user", null);
        }

        if (repositoryPrototype.isConfidential())
            if (userPrototype == null)
                return "error";
            else if (!(repositoryPrototype.getOwners().contains(userPrototype.getId())
                    || repositoryPrototype.getMembers().contains(userPrototype.getId())))
                return "error";

        context.put("repository", repositoryPrototype);
        context.put("namespace", namespace);
        context.put("urls", repositoryURLS(userPrototype, repositoryPrototype, request));

        try {
            Collection<FilePrototype> collection = gitRepositoryRawController.getTree(repositoryPrototype.getPath(), "/", getToken(request), null);
            CommitPrototype commitPrototype = null;

            for (FilePrototype prototype : collection) {
                if (commitPrototype == null) {
                    commitPrototype = prototype.commit;
                    continue;
                }

                if (commitPrototype.time <= prototype.commit.time)
                    commitPrototype = prototype.commit;
            }

            context.put("tree", collection);
            context.put("lastCommit", commitPrototype);
        } catch (Exception e) {
            context.put("tree", null);
            context.put("lastCommit", null);
        }

        return "repository/main";
    }
}
