package org.venity.vgit.frontend;

import org.eclipse.jgit.api.DiffCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.prototypes.CommitPrototype;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
public class RepositoryDiffViewController extends AbstractController {
    private final RepositoryDatabase repositoryDatabase;
    private final VGitConfigurationProperties configurationProperties;

    public RepositoryDiffViewController(UserDatabase userDatabase, RepositoryDatabase repositoryDatabase, VGitConfigurationProperties configurationProperties) {
        super(userDatabase, configurationProperties);
        this.repositoryDatabase = repositoryDatabase;
        this.configurationProperties = configurationProperties;
    }

    @GetMapping("/{namespace}/{repository}/diff/{commit}")
    public String fileView(HttpServletRequest request, Map<String, Object> context,
                           @PathVariable String namespace, @PathVariable String repository, @PathVariable String commit) {
        RepositoryPrototype repositoryPrototype
                = repositoryDatabase.findByPath(namespace + "/" + repository);

        if (repositoryPrototype == null)
            return "error";

        UserPrototype userPrototype = null;
        try {
            userPrototype = requireAuth(request);
            context.put("user", userPrototype);
        } catch (AuthenticationException ignored) {
            context.put("user", null);
        }

        Git git = null;
        try {
            git = Git.open(new File(configurationProperties.getRepositoryRoot(), repositoryPrototype.getPath()));
        } catch (Exception e) {
            context.put("error", e.getMessage());
            return "error";
        }

        if (repositoryPrototype.isConfidential())
            if (userPrototype == null)
                return "error";
            else if (!(repositoryPrototype.getOwners().contains(userPrototype.getId())
                    || repositoryPrototype.getMembers().contains(userPrototype.getId())))
                return "error";

        try {
            RevCommit c = git.getRepository().parseCommit(git.getRepository().resolve(commit));

            AbstractTreeIterator oldTree = null;

            if (c.getParentCount() >= 1)
                oldTree = prepareTreeParser(git.getRepository(), c.getParent(c.getParentCount() - 1).getName());

            AbstractTreeIterator newTree = prepareTreeParser(git.getRepository(),
                    commit);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            DiffCommand diffCommand = git.diff().setNewTree(newTree);

            if (oldTree != null)
                diffCommand.setOldTree(oldTree);

            List<DiffEntry> diff = diffCommand.call();
            for (DiffEntry entry : diff) {
                try (DiffFormatter formatter = new DiffFormatter(byteArrayOutputStream)) {
                    formatter.setRepository(git.getRepository());
                    formatter.format(entry);
                }
            }

            context.put("diff", byteArrayOutputStream.toString());
            context.put("commit", new CommitPrototype(git.getRepository().parseCommit(git.getRepository().resolve(commit))));
        } catch (Exception e) {
            context.put("error", e.getMessage());
            return "error";
        }

        context.put("repository", repositoryPrototype);
        context.put("namespace", namespace);
        context.put("urls", repositoryURLS(userPrototype, repositoryPrototype, request));

        return "repository/diffView";
    }

    private static AbstractTreeIterator prepareTreeParser(Repository repository, String objectId) throws IOException {
        try (RevWalk walk = new RevWalk(repository)) {
            RevCommit commit = walk.parseCommit(ObjectId.fromString(objectId));
            RevTree tree = walk.parseTree(commit.getTree().getId());

            CanonicalTreeParser treeParser = new CanonicalTreeParser();
            try (ObjectReader reader = repository.newObjectReader()) {
                treeParser.reset(reader, tree.getId());
            }

            walk.dispose();

            return treeParser;
        }
    }
}
