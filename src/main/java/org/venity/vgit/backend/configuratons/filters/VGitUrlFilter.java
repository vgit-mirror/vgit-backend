package org.venity.vgit.backend.configuratons.filters;

import org.eclipse.jgit.transport.RemoteConfig;
import org.springframework.stereotype.Component;
import org.venity.vgit.backend.git.servlets.VGitServlet;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class VGitUrlFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        if (httpServletRequest.getRequestURI().contains(".git")
                && !httpServletRequest.getRequestURI().startsWith(VGitServlet.URL_MAPPING)) {
            if (gitServiceIs(httpServletRequest, RemoteConfig.DEFAULT_RECEIVE_PACK) || httpServletRequest
                    .getRequestURI()
                    .contains(RemoteConfig.DEFAULT_RECEIVE_PACK)) {
                httpServletRequest
                        .getRequestDispatcher(VGitServlet.URL_MAPPING + httpServletRequest.getRequestURI())
                        .forward(httpServletRequest, httpServletResponse);

                return;
            } else httpServletRequest.getRequestDispatcher(VGitServlet.URL_MAPPING + httpServletRequest.getRequestURI())
                    .forward(request, response);
            return;
        }

        chain.doFilter(request, response);
    }

    private boolean gitServiceIs(HttpServletRequest httpServletRequest, String service) {
        try {
            return httpServletRequest.getParameter("service").equals(service);
        } catch (NullPointerException e) {
            return false;
        }
    }
}
