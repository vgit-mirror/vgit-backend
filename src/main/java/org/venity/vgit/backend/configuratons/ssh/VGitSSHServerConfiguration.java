package org.venity.vgit.backend.configuratons.ssh;

import lombok.extern.slf4j.Slf4j;
import org.apache.sshd.common.AttributeRepository;
import org.apache.sshd.common.file.nativefs.NativeFileSystemFactory;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;
import org.venity.vgit.backend.git.ssh.VGitCommandFactory;
import org.venity.vgit.backend.git.ssh.VGitPublickeyAuthenticator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.file.Path;

@Configuration
@Slf4j
@ConditionalOnProperty(name = "vgit.ssh.enabled", havingValue = "true")
@ConditionalOnBean(VGitConfigurationProperties.class)
public class VGitSSHServerConfiguration {
    private final UserDatabase userDatabase;
    private final VGitConfigurationProperties properties;
    private final VGitCommandFactory commandFactory;

    public static final AttributeRepository.AttributeKey<UserPrototype> USER_PROTOTYPE_ATTRIBUTE_KEY = new AttributeRepository.AttributeKey<>();

    public VGitSSHServerConfiguration(UserDatabase userDatabase, VGitConfigurationProperties properties, VGitCommandFactory commandFactory) {
        this.userDatabase = userDatabase;
        this.properties = properties;
        this.commandFactory = commandFactory;
    }

    @Bean
    public SshServer sshServer() {
        SshServer server = SshServer.setUpDefaultServer();
        server.setPort(properties.getSsh().getPort());
        server.setFileSystemFactory(NativeFileSystemFactory.INSTANCE);
        configureAuthenticationPolicies(server);
        configureServer(server);

        return server;
    }

    private void configureAuthenticationPolicies(SshServer server) {
        server.setKeyPairProvider(new SimpleGeneratorHostKeyProvider(Path.of("hostKey.ser")));
        server.setPublickeyAuthenticator(new VGitPublickeyAuthenticator(userDatabase));
    }

    private void configureServer(SshServer server) {
        server.setCommandFactory(commandFactory);
        configureShellFactory(server);
    }

    private void configureShellFactory(SshServer server) {
        server.setShellFactory(null);
    }

    @PostConstruct
    public void startServer() throws IOException {
        SshServer server = sshServer();
        server.start();
        log.info("SSH server started on port {}", server.getPort());
    }

    @PreDestroy
    public void stopServer() throws IOException {
        sshServer().stop();
        log.info("SSH server stopped");
    }
}
