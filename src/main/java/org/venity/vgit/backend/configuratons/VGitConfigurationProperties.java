package org.venity.vgit.backend.configuratons;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.PostConstruct;
import java.io.*;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = "vgit")
@Primary
@Slf4j
public class VGitConfigurationProperties {

    private static final String CONFIGURATION_FILE = ".configuration";
    private Map<String, Object> config = new HashMap<>();

    @ConfigurationField
    private File repositoryRoot;

    @ConfigurationField
    private VGitSSHConfigurationProperties ssh;

    @PostConstruct
    private void init() {
        File configFile = new File(CONFIGURATION_FILE);
        Map<String, Object> obj = null;

        if (!configFile.exists()) return;

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(configFile))) {
            obj = (Map<String, Object>) objectInputStream.readObject();

            for (Map.Entry<String, Object> entry: obj.entrySet()){
                try {
                    Field field = getClass().getField(entry.getKey());

                    if (!field.isAnnotationPresent(ConfigurationField.class))
                        continue;

                    field.setAccessible(true);
                    field.set(this, entry.getValue());
                } catch (NoSuchFieldException e) {
                    config.put(entry.getKey(), entry.getValue());
                } catch (Exception e) {
                    log.error("Error setting serialized value to field " + entry.getKey(), e);
                }
            }
        } catch (Exception e) {
            log.error("Error reading serialized configuration", e);
        }
    }

    public static void save(VGitConfigurationProperties configurationProperties) {
        File configFile = new File(CONFIGURATION_FILE);
        Map<String, Object> obj = configurationProperties.getConfig();

        List<Field> fields = Arrays.asList(VGitConfigurationProperties.class.getFields());

        for (Field field : fields) {
            System.err.println(field.getName());

            if (!field.isAnnotationPresent(ConfigurationField.class))
                continue;

            field.setAccessible(true);

            try {
                obj.put(field.getName(), field.get(configurationProperties));
            } catch (IllegalAccessException e) {
                log.error("Error saving configuration to serializable object", e);
            }
        }

        try {
            if (!configFile.exists()) configFile.createNewFile();

            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(configFile))) {
                objectOutputStream.writeObject(obj);
            } catch (NotSerializableException e) {

            }
        } catch (IOException e) {
            log.error("Error saving configuration to serializable object", e);
        }
    }

    public File getRepositoryRoot() {
        return repositoryRoot != null ? repositoryRoot : new File("./repo/");
    }

    @Data
    public static class VGitSSHConfigurationProperties implements Serializable {
        private Boolean enabled;
        private Integer port;
    }

    @Target(value = ElementType.FIELD)
    @Retention(value = RetentionPolicy.RUNTIME)
    @interface ConfigurationField {
    }
}
