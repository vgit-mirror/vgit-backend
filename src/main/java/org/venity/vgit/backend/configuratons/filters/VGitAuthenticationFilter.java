package org.venity.vgit.backend.configuratons.filters;

import org.eclipse.jgit.errors.RepositoryNotFoundException;
import org.eclipse.jgit.transport.RemoteConfig;
import org.springframework.stereotype.Component;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.git.VGitRepositoryResolver;
import org.venity.vgit.backend.git.servlets.VGitServlet;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class VGitAuthenticationFilter implements Filter {
    private final VGitRepositoryResolver repositoryResolver;

    public VGitAuthenticationFilter(VGitRepositoryResolver repositoryResolver) {
        this.repositoryResolver = repositoryResolver;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        if (httpServletRequest.getRequestURI().contains(".git") && !httpServletRequest.getRequestURI().startsWith(VGitServlet.URL_MAPPING)) {
            if (httpServletRequest
                    .getRequestURI()
                    .contains(RemoteConfig.DEFAULT_RECEIVE_PACK)) {
                chain.doFilter(request, response);
                return;
            }

            String repoPath = httpServletRequest.getRequestURI().substring(1, httpServletRequest.getRequestURI().indexOf(".git"));
            RepositoryPrototype repositoryPrototype = null;

            try {
                repositoryPrototype = repositoryResolver.open(repoPath).getPrototype();
            } catch (RepositoryNotFoundException e) {
                httpServletRequest.getRequestDispatcher("/error").forward(request, response);
                return;
            }

            if (repositoryPrototype.isConfidential() || isGitService(httpServletRequest, RemoteConfig.DEFAULT_RECEIVE_PACK))
                chain.doFilter(request, response);
            else httpServletRequest.getRequestDispatcher(VGitServlet.URL_MAPPING + httpServletRequest.getRequestURI())
                    .forward(request, response);

            return;
        }

        chain.doFilter(request, response);
    }

    private boolean isGitService(HttpServletRequest httpServletRequest, String service) {
        try {
            return httpServletRequest.getParameter("service").equals(service);
        } catch (NullPointerException e) {
            return false;
        }
    }
}
