package org.venity.vgit.backend.components;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.prototypes.UserPrototype;

import java.nio.charset.StandardCharsets;
import java.util.Collections;

import static com.google.common.hash.Hashing.sha256;

@Component
public class VGitAuthenticationProvider implements AuthenticationProvider {

    private final UserDatabase database;

    public VGitAuthenticationProvider(UserDatabase database) {
        this.database = database;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            String login = authentication.getName();
            String password = authentication.getCredentials().toString();

            UserPrototype prototype = database.findByLogin(login);

            if (prototype == null)
                throw new BadCredentialsException("User not found!");

            if (!prototype.getPassword().equals(sha256().hashString(password, StandardCharsets.UTF_8).toString()))
                throw new BadCredentialsException("Incorrect password!");

            return new UsernamePasswordAuthenticationToken(login, password, Collections.emptyList());
        } catch (NullPointerException e) {
            throw new BadCredentialsException("");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
