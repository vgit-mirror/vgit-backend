package org.venity.vgit.backend.components;

import org.eclipse.jgit.revwalk.RevCommit;
import org.springframework.stereotype.Component;
import org.venity.vgit.backend.api.prototypes.ProfileActivityPrototype;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Component
public class VGitProfileActivityHelper {
    private final static String dataFileName = "profileActivity";

    public static ProfileActivityPrototype get(VGitConfigurationProperties configurationProperties, UserPrototype userPrototype) {
        try (FileInputStream in = new FileInputStream(getDataFile(configurationProperties, userPrototype))) {
            try (ObjectInputStream objIn = new ObjectInputStream(in)) {
                return (ProfileActivityPrototype) objIn.readObject();
            }
        } catch (Exception ignored) {
        }

        return new ProfileActivityPrototype(userPrototype.getId(), new HashMap<>());
    }

    public static void save(VGitConfigurationProperties configurationProperties, ProfileActivityPrototype activityPrototype, UserPrototype userPrototype) throws IOException {
        File dataFile = getDataFileNoThrow(configurationProperties, userPrototype);
        if (!dataFile.exists()) dataFile.createNewFile();

        try (FileOutputStream fileOutputStream = new FileOutputStream(dataFile)) {
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
                objectOutputStream.writeObject(activityPrototype);
            }
        }
    }

    private static File getDataFileNoThrow(VGitConfigurationProperties configurationProperties, UserPrototype userPrototype) {
        return new File(configurationProperties.getRepositoryRoot(),
                new File(userPrototype.getLogin(), dataFileName).getPath());
    }

    private static File getDataFile(VGitConfigurationProperties configurationProperties, UserPrototype userPrototype) throws IOException {
        File dataFile = getDataFileNoThrow(configurationProperties, userPrototype);
        if (!dataFile.exists())
            throw new IOException();

        return dataFile;
    }

    public static ProfileActivityPrototype.ProfileActivityElementPrototype createFromCommit(
            RepositoryPrototype repositoryPrototype, RevCommit commit) {
        Map<String, String> params = new HashMap<>();
        params.put("time", String.valueOf(commit.getCommitTime()));
        params.put("commit", commit.getName());
        params.put("repository", repositoryPrototype.getPath());

        return new ProfileActivityPrototype.ProfileActivityElementPrototype("COMMIT", params);
    }
}
