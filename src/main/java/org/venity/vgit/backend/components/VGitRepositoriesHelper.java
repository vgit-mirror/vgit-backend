package org.venity.vgit.backend.components;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import java.io.File;

public class VGitRepositoriesHelper {

    public static String normalize(String name) {
        return normalize(name, false);
    }

    public static String normalize(String name, boolean cutSlash) {
        name = name.trim().replace(" ", "-");

        if (name.endsWith(".git"))
            name = name.substring(0, name.length() - 4);

        if (cutSlash) {
            if (name.contains("/"))
                name = name.substring(0, name.indexOf("/"));
        }

        return name;
    }

    public static RepositoryPrototype create(
            VGitConfigurationProperties configurationProperties, String name, String repoPath,
            String description, Long owner, Boolean confidential) throws APIMethodException {
        File repo = new File(configurationProperties.getRepositoryRoot(), repoPath);

        if (repo.exists())
            throw APIMethodException.ERRORS.REPOSITORY_CREATE_EXISTS.exception();

        RepositoryPrototype repositoryPrototype
                = new RepositoryPrototype(name, repoPath, description, owner, confidential);

        if (!repo.mkdirs())
            throw APIMethodException.ERRORS.REPOSITORY_CREATE_ERROR.exception();

        try {
            Git.init()
                    .setDirectory(repo)
                    .call();
        } catch (GitAPIException e) {
            e.printStackTrace();
            throw APIMethodException.ERRORS.REPOSITORY_CREATE_ERROR.exception();
        }

        return repositoryPrototype;
    }
}
