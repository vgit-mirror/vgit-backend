package org.venity.vgit.backend.git.ssh;

import org.apache.sshd.common.config.keys.AuthorizedKeyEntry;
import org.apache.sshd.server.auth.AsyncAuthException;
import org.apache.sshd.server.auth.pubkey.PublickeyAuthenticator;
import org.apache.sshd.server.session.ServerSession;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.api.prototypes.UserPublicKeyPrototype;

import java.security.PublicKey;

import static org.venity.vgit.backend.configuratons.ssh.VGitSSHServerConfiguration.USER_PROTOTYPE_ATTRIBUTE_KEY;

public class VGitPublickeyAuthenticator implements PublickeyAuthenticator {
    private final UserDatabase userDatabase;

    public VGitPublickeyAuthenticator(UserDatabase userDatabase) {
        this.userDatabase = userDatabase;
    }

    @Override
    public boolean authenticate(String username, PublicKey key, ServerSession session) throws AsyncAuthException {
        UserPrototype userPrototype = userDatabase.findByLogin(username);

        if (userPrototype == null)
            throw new AsyncAuthException();

        for (UserPublicKeyPrototype publicKeyPrototype: userPrototype.getPublicKeys()) {
            if (publicKeyPrototype.getRawKey().equals(AuthorizedKeyEntry.toString(key))) {
                session.setAttribute(USER_PROTOTYPE_ATTRIBUTE_KEY, userPrototype);
                return true;
            }
        }

        return false;
    }
}
