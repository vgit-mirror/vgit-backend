package org.venity.vgit.backend.git.ssh;

import org.apache.sshd.server.command.AbstractCommandSupport;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.RemoteConfig;
import org.venity.vgit.backend.git.transport.VGitUploadPack;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Collections;

public class VGitUploadPackCommand extends AbstractCommandSupport {
    private final Repository repository;

    public VGitUploadPackCommand(Repository repository) {
        super(RemoteConfig.DEFAULT_UPLOAD_PACK, null);
        this.repository = repository;
    }

    @Override
    public void run() {
        VGitUploadPack uploadPack = new VGitUploadPack(repository);
        String gitProtocol = getEnvironment().getEnv().get("GIT_PROTOCOL");
        if (gitProtocol != null) {
            uploadPack
                    .setExtraParameters(Collections.singleton(gitProtocol));
        }
        try {
            uploadPack.upload(getInputStream(), getOutputStream(), getErrorStream());
            onExit(0);
        } catch (IOException e) {
            log.warn(MessageFormat.format("Could not run {0}", getCommand()), e);
            onExit(-1, e.toString());
        }
    }
}