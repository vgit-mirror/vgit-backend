package org.venity.vgit.backend.git;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.errors.RepositoryNotFoundException;
import org.eclipse.jgit.lib.Repository;
import org.springframework.stereotype.Component;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.components.VGitRepositoriesHelper;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import java.io.File;
import java.io.IOException;

@Component
@Slf4j
public class VGitRepositoryResolver {
    private final RepositoryDatabase repositoryDatabase;
    private final VGitConfigurationProperties configurationProperties;

    public VGitRepositoryResolver(RepositoryDatabase repositoryDatabase, VGitConfigurationProperties configurationProperties) {
        this.repositoryDatabase = repositoryDatabase;
        this.configurationProperties = configurationProperties;
    }

    public VGitRepository open(String name) throws RepositoryNotFoundException {
        name = VGitRepositoriesHelper.normalize(name);

        RepositoryPrototype repositoryPrototype = repositoryDatabase.findByPath(name);

        if (repositoryPrototype == null)
            throw new RepositoryNotFoundException(name);

        File repo = new File(configurationProperties.getRepositoryRoot(), name);

        if (!repo.exists()) {
            throw new RepositoryNotFoundException(repo);
        }

        try {
            return new VGitRepository(Git.open(repo).getRepository(), repositoryPrototype);
        } catch (IOException e) {
            log.warn("Can`t open repository {}", name, e);
            throw new RepositoryNotFoundException(repo);
        }
    }

    @Data
    public static class VGitRepository {
        private final Repository repository;
        private final RepositoryPrototype prototype;

        public VGitRepository(Repository repository, RepositoryPrototype prototype) {
            this.repository = repository;
            this.prototype = prototype;
        }
    }
}
