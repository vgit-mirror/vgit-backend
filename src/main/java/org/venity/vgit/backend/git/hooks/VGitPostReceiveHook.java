package org.venity.vgit.backend.git.hooks;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LogCommand;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.PostReceiveHook;
import org.eclipse.jgit.transport.ReceiveCommand;
import org.eclipse.jgit.transport.ReceivePack;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.prototypes.ProfileActivityPrototype;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.components.VGitProfileActivityHelper;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import java.io.IOException;
import java.util.*;

public class VGitPostReceiveHook implements PostReceiveHook {

    private final UserDatabase database;
    private final RepositoryPrototype repositoryPrototype;
    private final VGitConfigurationProperties configurationProperties;

    public VGitPostReceiveHook(UserDatabase database, RepositoryPrototype prototype, VGitConfigurationProperties configurationProperties) {
        this.database = database;
        this.repositoryPrototype = prototype;
        this.configurationProperties = configurationProperties;
    }

    @Override
    public void onPostReceive(ReceivePack rp, Collection<ReceiveCommand> commands) {
        List<RevCommit> commits = new ArrayList<>();

        for (ReceiveCommand command: commands) {
            try {
                LogCommand log = Git.wrap(rp.getRepository()).log();

                if (command.getType().equals(ReceiveCommand.Type.UPDATE)
                        || command.getType().equals(ReceiveCommand.Type.UPDATE_NONFASTFORWARD))
                    log.not(command.getOldId());

                log.add(command.getNewId()).call().forEach(commits::add);
            } catch (Exception ignored) {
            }
        }

        Map<String, UserPrototype> userPrototypeHashMap = new HashMap<>();
        Map<String, ProfileActivityPrototype> activityPrototypeMap = new HashMap<>();

        for (RevCommit commit: commits) {
            UserPrototype prototype = null;
            ProfileActivityPrototype activityPrototype = null;
            String email = commit.getAuthorIdent().getEmailAddress();

            if (userPrototypeHashMap.containsKey(email))
                prototype = userPrototypeHashMap.get(email);
            else {
                prototype = database.findByEmail(email);

                if (prototype != null)
                    userPrototypeHashMap.put(email, prototype);
            }

            if (prototype == null) continue;

            if (activityPrototypeMap.containsKey(email))
                activityPrototype = activityPrototypeMap.get(email);
            else {
                activityPrototype = VGitProfileActivityHelper.get(configurationProperties, prototype);
                activityPrototypeMap.put(email, activityPrototype);
            }

            Map<Integer, ProfileActivityPrototype.ProfileActivityElementPrototype> elementPrototypeMap
                    = activityPrototype.getElements();
            elementPrototypeMap.put(commit.getCommitTime(),
                    VGitProfileActivityHelper.createFromCommit(repositoryPrototype, commit));
            activityPrototype.setElements(elementPrototypeMap);
        }

        for (Map.Entry<String, ProfileActivityPrototype> profileActivityEntry: activityPrototypeMap.entrySet()) {
            try {
                VGitProfileActivityHelper.save(
                        configurationProperties, profileActivityEntry.getValue(), userPrototypeHashMap.get(profileActivityEntry.getKey()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.gc();
    }
}
