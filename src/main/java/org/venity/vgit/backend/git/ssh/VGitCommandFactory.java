package org.venity.vgit.backend.git.ssh;

import org.apache.sshd.git.pack.GitPackCommandFactory;
import org.apache.sshd.server.channel.ChannelSession;
import org.apache.sshd.server.command.Command;
import org.apache.sshd.server.command.CommandFactory;
import org.apache.sshd.server.shell.UnknownCommand;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.RemoteConfig;
import org.springframework.stereotype.Component;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.components.VGitRepositoriesHelper;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;
import org.venity.vgit.backend.git.VGitRepositoryResolver;

import java.io.IOException;

import static org.venity.vgit.backend.configuratons.ssh.VGitSSHServerConfiguration.USER_PROTOTYPE_ATTRIBUTE_KEY;

@Component
public class VGitCommandFactory implements CommandFactory {
    private final VGitRepositoryResolver repositoryResolver;
    private final UserDatabase userDatabase;
    private final VGitConfigurationProperties properties;

    public VGitCommandFactory(VGitRepositoryResolver repositoryResolver, UserDatabase userDatabase, VGitConfigurationProperties properties) {
        this.repositoryResolver = repositoryResolver;
        this.userDatabase = userDatabase;
        this.properties = properties;
    }

    @Override
    public Command createCommand(ChannelSession channel, String command) throws IOException {
        if (channel.getSession().getAttribute(USER_PROTOTYPE_ATTRIBUTE_KEY) == null)
            throw new IOException();

        UserPrototype userPrototype = channel.getSession().getAttribute(USER_PROTOTYPE_ATTRIBUTE_KEY);

        if (command.startsWith(GitPackCommandFactory.GIT_COMMAND_PREFIX)) {
            String path = command.split(" ")[1];

            if (path.startsWith("'"))
                path = path.substring(1);

            if (path.endsWith("'"))
                path = path.substring(0, path.length() - 1);

            if (path.startsWith("/"))
                path = path.substring(1);

            path = VGitRepositoriesHelper.normalize(path);

            VGitRepositoryResolver.VGitRepository gitRepository = repositoryResolver.open(path);
            RepositoryPrototype repositoryPrototype = gitRepository.getPrototype();
            Repository repository = gitRepository.getRepository();

            if (command.startsWith(RemoteConfig.DEFAULT_UPLOAD_PACK)) {
                if (!repositoryPrototype.isConfidential() || (repositoryPrototype.getMembers().contains(userPrototype.getId()) ||
                        repositoryPrototype.getOwners().contains(userPrototype.getId())))
                    return new VGitUploadPackCommand(repository);
                else throw new IOException();
            } else if (command.startsWith(RemoteConfig.DEFAULT_RECEIVE_PACK)) {
                if (repositoryPrototype.getMembers().contains(userPrototype.getId()) ||
                        repositoryPrototype.getOwners().contains(userPrototype.getId()))
                    return new VGitReceivePackCommand(repository, userDatabase, repositoryPrototype, properties);
                else throw new IOException();
            }
        }

        return new UnknownCommand(command);
    }
}
