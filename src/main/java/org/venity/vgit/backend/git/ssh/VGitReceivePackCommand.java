package org.venity.vgit.backend.git.ssh;

import org.apache.sshd.server.command.AbstractCommandSupport;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.RemoteConfig;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;
import org.venity.vgit.backend.git.transport.VGitReceivePack;

import java.io.IOException;
import java.text.MessageFormat;

public class VGitReceivePackCommand extends AbstractCommandSupport {
    private final Repository repository;
    private final UserDatabase userDatabase;
    private final RepositoryPrototype repositoryPrototype;
    private final VGitConfigurationProperties configurationProperties;

    public VGitReceivePackCommand
            (Repository repository, UserDatabase userDatabase, RepositoryPrototype repositoryPrototype, VGitConfigurationProperties configurationProperties) {
        super(RemoteConfig.DEFAULT_RECEIVE_PACK, null);

        this.repository = repository;
        this.userDatabase = userDatabase;
        this.repositoryPrototype = repositoryPrototype;
        this.configurationProperties = configurationProperties;
    }

    @Override
    public void run() {
        try {
            VGitReceivePack receivePack = new VGitReceivePack(repository, userDatabase, repositoryPrototype, configurationProperties);
            receivePack.receive(getInputStream(), getOutputStream(), getErrorStream());
            onExit(0);
        } catch (IOException e) {
            log.warn(
                    MessageFormat.format("Could not run {0}", getCommand()),
                    e);
            onExit(-1, e.toString());
        }
    }
}