package org.venity.vgit.backend.git.servlets;

import org.eclipse.jgit.http.server.GitFilter;
import org.eclipse.jgit.http.server.GitServlet;
import org.eclipse.jgit.transport.resolver.ServiceNotAuthorizedException;
import org.eclipse.jgit.transport.resolver.ServiceNotEnabledException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;
import org.venity.vgit.backend.git.VGitRepositoryResolver;
import org.venity.vgit.backend.git.transport.VGitReceivePack;
import org.venity.vgit.backend.git.transport.VGitUploadPack;

import java.util.HashMap;
import java.util.Map;

@Component
public class VGitServlet extends GitServlet {
    private final Map<String, RepositoryPrototype> repositoryPrototypeMap = new HashMap<>();

    public static final String SERVLET_URL_MAPPING = "/git/repository/*";
    public static final String URL_MAPPING = "/git/repository";

    public VGitServlet(UserDatabase userDatabase, VGitConfigurationProperties configurationProperties, VGitRepositoryResolver repositoryResolver) {
        super();

        setUploadPackFactory(((req, db) -> new VGitUploadPack(db)));
        setReceivePackFactory((req, db) -> {
            String login = null;

            try {
                login = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            } catch (NullPointerException e) {
                return new VGitReceivePack(db, userDatabase, repositoryPrototypeMap.get(db.toString()), configurationProperties);
            }

            if (login == null)
                throw new ServiceNotAuthorizedException();

            UserPrototype userPrototype = userDatabase.findByLogin(login);
            RepositoryPrototype repositoryPrototype = repositoryPrototypeMap.get(db.toString());

            try {
                if (!(repositoryPrototype.getOwners().contains(userPrototype.getId())
                        || repositoryPrototype.getMembers().contains(userPrototype.getId()))) {
                    throw new ServiceNotEnabledException();
                }
            } catch (NullPointerException e) {
                throw new ServiceNotEnabledException();
            }

            return new VGitReceivePack(db, userDatabase, repositoryPrototype, configurationProperties);
        });

        setRepositoryResolver((req, name) -> {
            VGitRepositoryResolver.VGitRepository repository = repositoryResolver.open(name);
            repositoryPrototypeMap.put(repository.getRepository().toString(), repository.getPrototype());
            return repository.getRepository();
        });
    }

    public GitFilter getFilter() {
        return (GitFilter) super.getDelegateFilter();
    }
}
