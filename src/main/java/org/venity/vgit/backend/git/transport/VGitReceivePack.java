package org.venity.vgit.backend.git.transport;

import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.ReceivePack;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;
import org.venity.vgit.backend.git.hooks.VGitPostReceiveHook;

public class VGitReceivePack extends ReceivePack {
    public VGitReceivePack(Repository into, UserDatabase userDatabase, RepositoryPrototype repositoryPrototype, VGitConfigurationProperties configurationProperties) {
        super(into);

        setPostReceiveHook(new VGitPostReceiveHook(userDatabase, repositoryPrototype, configurationProperties));
    }
}
