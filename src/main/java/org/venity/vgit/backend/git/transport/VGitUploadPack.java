package org.venity.vgit.backend.git.transport;

import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.UploadPack;

public class VGitUploadPack extends UploadPack {
    public VGitUploadPack(Repository copyFrom) {
        super(copyFrom);
    }
}
