package org.venity.vgit.backend.api.controllers.group;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.controllers.repository.GitRepositoryController;
import org.venity.vgit.backend.api.database.GroupDatabase;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.GroupPrototype;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.components.VGitRepositoriesHelper;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api/group")
@Slf4j
public class GroupController implements APIController {
    private final GroupDatabase groupDatabase;
    private final UserDatabase userDatabase;
    private final RepositoryDatabase repositoryDatabase;
    private final GitRepositoryController gitRepositoryController;

    public GroupController(GroupDatabase groupDatabase, UserDatabase userDatabase, RepositoryDatabase repositoryDatabase, GitRepositoryController gitRepositoryController) {
        this.groupDatabase = groupDatabase;
        this.userDatabase = userDatabase;
        this.repositoryDatabase = repositoryDatabase;
        this.gitRepositoryController = gitRepositoryController;
    }

    @GetMapping("/create")
    public GroupPrototype create(String name, String description, String token) throws APIMethodException {
        if (name == null)
            throw APIMethodException.ERRORS.GROUP_NAME_MISMATCH.exception();

        if (description == null)
            description = "";

        name = VGitRepositoriesHelper.normalize(name, true);

        UserPrototype userPrototype = requireAuth(userDatabase, token);

        if (groupDatabase.findByName(name) != null)
            throw APIMethodException.ERRORS.GROUP_EXISTS.exception();

        groupDatabase.save(new GroupPrototype(name, description, userPrototype.getId()));
        return groupDatabase.findByNameAndOwnersIn(name, userPrototype.getId());
    }

    @GetMapping("/remove")
    public Map<String, Boolean> remove(String group, String token) throws APIMethodException {
        if (group == null)
            throw APIMethodException.ERRORS.GROUP_NAME_MISMATCH.exception();

        group = VGitRepositoriesHelper.normalize(group, true);
        UserPrototype userPrototype = requireAuth(userDatabase, token);
        GroupPrototype groupPrototype = groupDatabase.findByNameAndOwnersIn(group, userPrototype.getId());

        if (groupPrototype == null)
            throw APIMethodException.ERRORS.GROUP_ACCESS_DENIED.exception();

        for (Long repoId : groupPrototype.getRepositories()) {
            try {
                RepositoryPrototype repository = repositoryDatabase.getOne(repoId);
                gitRepositoryController.removeRepo(token, repository.getPath());
            } catch (Exception e) {
                log.warn("Error removing repository", e);
            }
        }

        groupDatabase.delete(groupPrototype);

        return Collections.singletonMap("ok", true);
    }

    @GetMapping("/get")
    public GroupPrototype get(Long id) throws APIMethodException {
        return groupDatabase.findById(id)
                .orElseThrow(APIMethodException.ERRORS.GROUP_NOT_FOUND::exception);
    }

    @GetMapping("/get/name")
    public GroupPrototype getByName(String name) throws APIMethodException {
        return groupDatabase.findOptByName(name)
                .orElseThrow(APIMethodException.ERRORS.GROUP_NOT_FOUND::exception);
    }

    @GetMapping("/add/member")
    public Map<String, Boolean> addMember(String group, Long user, String token) throws APIMethodException {
        if (user == null)
            throw APIMethodException.ERRORS.USER_GET_ID_MISMATCH.exception();

        UserPrototype userPrototype = requireAuth(userDatabase, token);
        UserPrototype memberPrototype = userDatabase.findById(user)
                .orElseThrow(APIMethodException.ERRORS.USER_GET_NOTFOUND::exception);

        if (userPrototype.getId().equals(memberPrototype.getId()))
            return Collections.singletonMap("ok", false);

        GroupPrototype groupPrototype = groupDatabase.findByNameAndOwnersIn(group, userPrototype.getId());

        if (groupPrototype == null)
            throw APIMethodException.ERRORS.GROUP_NOT_FOUND.exception();

        Set<Long> members = groupPrototype.getMembers();
        members.add(user);
        groupPrototype.setMembers(members);
        groupDatabase.save(groupPrototype);

        for (Long repoId : groupPrototype.getRepositories()) {
            RepositoryPrototype repository = repositoryDatabase.getOne(repoId);
            Set<Long> repositoryMembers = repository.getMembers();
            repositoryMembers.add(user);
            repository.setMembers(repositoryMembers);
            repositoryDatabase.save(repository);
        }

        return Collections.singletonMap("ok", true);
    }

    @GetMapping("/remove/member")
    public Map<String, Boolean> removeMember(String group, Long user, String token) throws APIMethodException {
        if (user == null)
            throw APIMethodException.ERRORS.USER_GET_ID_MISMATCH.exception();

        UserPrototype userPrototype = requireAuth(userDatabase, token);
        UserPrototype memberPrototype = userDatabase.findById(user)
                .orElseThrow(APIMethodException.ERRORS.USER_GET_NOTFOUND::exception);

        if (userPrototype.getId().equals(memberPrototype.getId()))
            return Collections.singletonMap("ok", false);

        GroupPrototype groupPrototype = groupDatabase.findByNameAndOwnersIn(group, userPrototype.getId());

        if (groupPrototype == null)
            throw APIMethodException.ERRORS.GROUP_NOT_FOUND.exception();

        Set<Long> members = groupPrototype.getMembers();
        members.remove(user);
        groupPrototype.setMembers(members);
        groupDatabase.save(groupPrototype);

        for (Long repoId : groupPrototype.getRepositories()) {
            RepositoryPrototype repository = repositoryDatabase.getOne(repoId);
            Set<Long> repositoryMembers = repository.getMembers();
            repositoryMembers.remove(user);
            repository.setMembers(repositoryMembers);
            repositoryDatabase.save(repository);
        }

        return Collections.singletonMap("ok", true);
    }

    @GetMapping("/add/owner")
    public Map<String, Boolean> addOwner(String group, Long user, String token) throws APIMethodException {
        if (user == null)
            throw APIMethodException.ERRORS.USER_GET_ID_MISMATCH.exception();

        UserPrototype userPrototype = requireAuth(userDatabase, token);
        UserPrototype ownerPrototype = userDatabase.findById(user)
                .orElseThrow(APIMethodException.ERRORS.USER_GET_NOTFOUND::exception);

        if (userPrototype.getId().equals(ownerPrototype.getId()))
            return Collections.singletonMap("ok", false);

        GroupPrototype groupPrototype = groupDatabase.findByNameAndOwnersIn(group, userPrototype.getId());

        if (groupPrototype == null)
            throw APIMethodException.ERRORS.GROUP_NOT_FOUND.exception();

        Set<Long> owners = groupPrototype.getOwners();
        owners.add(user);
        groupPrototype.setOwners(owners);
        groupDatabase.save(groupPrototype);

        for (Long repoId : groupPrototype.getRepositories()) {
            RepositoryPrototype repository = repositoryDatabase.getOne(repoId);
            Set<Long> repositoryOwners = repository.getOwners();
            repositoryOwners.add(user);
            repository.setMembers(repositoryOwners);
            repositoryDatabase.save(repository);
        }

        return Collections.singletonMap("ok", true);
    }

    @GetMapping("/remove/owner")
    public Map<String, Boolean> removeOwner(String group, Long user, String token) throws APIMethodException {
        if (user == null)
            throw APIMethodException.ERRORS.USER_GET_ID_MISMATCH.exception();

        UserPrototype userPrototype = requireAuth(userDatabase, token);
        UserPrototype ownerPrototype = userDatabase.findById(user)
                .orElseThrow(APIMethodException.ERRORS.USER_GET_NOTFOUND::exception);

        if (userPrototype.getId().equals(ownerPrototype.getId()))
            return Collections.singletonMap("ok", false);

        GroupPrototype groupPrototype = groupDatabase.findByNameAndOwnersIn(group, userPrototype.getId());

        if (groupPrototype == null)
            throw APIMethodException.ERRORS.GROUP_NOT_FOUND.exception();

        Set<Long> owners = groupPrototype.getOwners();
        owners.remove(user);
        groupPrototype.setOwners(owners);
        groupDatabase.save(groupPrototype);

        for (Long repoId : groupPrototype.getRepositories()) {
            RepositoryPrototype repository = repositoryDatabase.getOne(repoId);
            Set<Long> repositoryOwners = repository.getOwners();
            repositoryOwners.remove(user);
            repository.setMembers(repositoryOwners);
            repositoryDatabase.save(repository);
        }

        return Collections.singletonMap("ok", true);
    }
}
