package org.venity.vgit.backend.api.controllers.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.UserPrototype;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.google.common.hash.Hashing.*;

@RestController
@RequestMapping("/api/user")
public class UserAuthController implements APIController {

    private final UserDatabase database;

    public UserAuthController(UserDatabase database) {
        this.database = database;
    }

    @GetMapping("/login")
    public Map<String, String> login(String login, String password) throws APIMethodException {
        if (login == null)
            throw APIMethodException.ERRORS.AUTHORIZATION_LOGIN_MISMATCH.exception();

        if (password == null)
            throw APIMethodException.ERRORS.AUTHORIZATION_PASSWORD_MISMATCH.exception();

        UserPrototype prototype = database.findByLogin(login);

        if (prototype == null)
            throw APIMethodException.ERRORS.AUTHORIZATION_INVALID_LOGIN.exception();

        if (!sha256()
                .hashString(password, StandardCharsets.UTF_8)
                .toString()
                .equals(prototype.getPassword()))
            throw APIMethodException.ERRORS.AUTHORIZATION_INVALID_PASSWORD.exception();

        String token = sha256()
                .hashString(
                        login + prototype.getPassword() + System.currentTimeMillis() % 1000,
                        StandardCharsets.UTF_8)
                .toString();

        Set<String> tokens = prototype.getTokens();

        if (tokens == null)
            tokens = new HashSet<>();

        tokens.add(token);
        prototype.setTokens(tokens);
        database.save(prototype);

        return Collections.singletonMap("token", token);
    }

    @GetMapping("/logout")
    public Map<String, Boolean> logout(String token) throws APIMethodException {
        UserPrototype prototype = requireAuth(database, token);
        Set<String> tokens = prototype.getTokens();
        tokens.remove(token);
        prototype.setTokens(tokens);
        database.save(prototype);

        return Collections.singletonMap("ok", true);
    }
}
