package org.venity.vgit.backend.api.prototypes;

import lombok.Data;

import javax.persistence.*;
import java.util.Collections;
import java.util.Set;

@Entity
@Data
public class GroupPrototype {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String description;

    private String url;

    @ElementCollection(targetClass = Long.class)
    @JoinColumn(name = "group_repositories")
    private Set<Long> repositories;

    @ElementCollection(targetClass = Long.class, fetch = FetchType.EAGER)
    private Set<Long> owners;

    @ElementCollection(targetClass = Long.class, fetch = FetchType.EAGER)
    private Set<Long> members;

    public GroupPrototype() {

    }

    public GroupPrototype(String name, String description, Long owner) {
        this.name = name;
        this.description = description;
        this.owners = Collections.singleton(owner);
        this.members = Collections.emptySet();
        this.repositories = Collections.emptySet();
    }
}
