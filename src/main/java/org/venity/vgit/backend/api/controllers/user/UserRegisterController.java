package org.venity.vgit.backend.api.controllers.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.UserPrototype;

@RestController
@RequestMapping("/api/user/registration")
public class UserRegisterController implements APIController {

    private final UserDatabase database;

    public UserRegisterController(UserDatabase database) {
        this.database = database;
    }

    @GetMapping
    public UserPrototype reg(String login, String fullName, String email, String password) throws APIMethodException {
        if (login == null)
            throw APIMethodException.ERRORS.REGISTRATION_LOGIN_MISMATCH.exception();

        if (fullName == null)
            throw APIMethodException.ERRORS.REGISTRATION_NAME_MISMATCH.exception();

        if (email == null)
            throw APIMethodException.ERRORS.REGISTRATION_EMAIL_MISMATCH.exception();

        if (password == null)
            throw APIMethodException.ERRORS.REGISTRATION_PASSWORD_MISMATCH.exception();

        if (database.findByLogin(login) != null)
            throw APIMethodException.ERRORS.REGISTRATION_LOGIN_EXIST.exception();

        if (database.findByEmail(email) != null)
            throw APIMethodException.ERRORS.REGISTRATION_EMAIL_EXIST.exception();

        if (!email.matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$"))
            throw APIMethodException.ERRORS.REGISTRATION_INVALID_EMAIL.exception();

        if (!login.matches("^(?=.{5,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$"))
            throw APIMethodException.ERRORS.REGISTRATION_INVALID_LOGIN.exception();

        if (password.length() < 8)
            throw APIMethodException.ERRORS.REGISTRATION_INVALID_PASSWORD.exception();

        return database.save(
                new UserPrototype(login, fullName, email, "USER", password));
    }
}
