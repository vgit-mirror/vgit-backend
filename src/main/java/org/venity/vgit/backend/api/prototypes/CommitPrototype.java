package org.venity.vgit.backend.api.prototypes;

import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.revwalk.RevCommit;

public class CommitPrototype {

    public String parent;
    public String name;
    public CommitAuthor author;
    public int time;
    public String message;
    public String fullMessage;

    public CommitPrototype(RevCommit commit, String parent) {
        this.name = commit.getName();
        this.parent = parent;
        this.message = commit.getShortMessage();
        this.fullMessage = commit.getFullMessage();
        this.time = commit.getCommitTime();
        this.author = new CommitAuthor(commit.getAuthorIdent());
    }

    public CommitPrototype(RevCommit commit) {
        this.name = commit.getName();
        this.parent = null;
        this.message = commit.getShortMessage();
        this.fullMessage = commit.getFullMessage();
        this.time = commit.getCommitTime();
        this.author = new CommitAuthor(commit.getAuthorIdent());

        if (commit.getParentCount() >= 1)
            this.parent = commit.getParent(0).getName();
    }

    class CommitAuthor {
        public String name;
        public String email;

        public CommitAuthor(PersonIdent authorIdent) {
            this.name = authorIdent.getName();
            this.email = authorIdent.getEmailAddress();
        }
    }
}
