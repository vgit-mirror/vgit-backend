package org.venity.vgit.backend.api.database.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.venity.vgit.backend.api.prototypes.UserPublicKeyPrototype;

public interface UserPublicKeyDatabase extends JpaRepository<UserPublicKeyPrototype, Long> {
}
