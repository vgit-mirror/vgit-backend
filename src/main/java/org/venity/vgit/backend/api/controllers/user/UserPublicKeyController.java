package org.venity.vgit.backend.api.controllers.user;

import org.apache.sshd.common.config.keys.AuthorizedKeyEntry;
import org.apache.sshd.common.config.keys.PublicKeyEntryResolver;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.database.user.UserPublicKeyDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.exceptions.ExceptionEntry;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.api.prototypes.UserPublicKeyPrototype;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api/user/ssh")
public class UserPublicKeyController implements APIController {

    private final UserDatabase userDatabase;
    private final UserPublicKeyDatabase publicKeyDatabase;

    public UserPublicKeyController(UserDatabase userDatabase, UserPublicKeyDatabase publicKeyDatabase) {
        this.userDatabase = userDatabase;
        this.publicKeyDatabase = publicKeyDatabase;
    }

    @PostMapping("/addKey")
    public Map<String, Boolean> add(String name, String token, MultipartFile keyFile) throws IOException, GeneralSecurityException, APIMethodException {
        UserPrototype userPrototype = requireAuth(userDatabase, token);
        AuthorizedKeyEntry entry = AuthorizedKeyEntry
                .readAuthorizedKeys(keyFile.getInputStream(), true)
                .get(0);

        if (name == null)
            name = entry.getComment();

        UserPublicKeyPrototype keyPrototype
                = new UserPublicKeyPrototype(name, AuthorizedKeyEntry.toString(
                        entry.resolvePublicKey(null, PublicKeyEntryResolver.IGNORING)));
        publicKeyDatabase.save(keyPrototype);

        Set<UserPublicKeyPrototype> userPublicKeyPrototypes = userPrototype.getPublicKeys();
        userPublicKeyPrototypes.add(keyPrototype);
        userPrototype.setPublicKeys(userPublicKeyPrototypes);
        userDatabase.save(userPrototype);

        return Collections.singletonMap("ok", true);
    }

    @GetMapping("/removeKey")
    public Map<String, Boolean> remove(Long id, String token) throws APIMethodException {
        UserPrototype userPrototype = requireAuth(userDatabase, token);
        Set<UserPublicKeyPrototype> publicKeyPrototypes = userPrototype.getPublicKeys();

        for (UserPublicKeyPrototype publicKey : publicKeyPrototypes) {
            if (publicKey.getId().equals(id)) {
                publicKeyPrototypes.remove(publicKey);
                return Collections.singletonMap("ok", true);
            }
        }

        return Collections.singletonMap("ok", false);
    }

    @GetMapping("/listKeys")
    public Set<UserPublicKeyPrototype> getKeys(String token) throws APIMethodException {
        return requireAuth(userDatabase, token).getPublicKeys();
    }

    @ExceptionHandler(GeneralSecurityException.class)
    public ResponseEntity<ExceptionEntry> generalSecurityException(GeneralSecurityException e) {
        return ResponseEntity.status(500).body(new ExceptionEntry(-1, e.getMessage()));
    }
}
