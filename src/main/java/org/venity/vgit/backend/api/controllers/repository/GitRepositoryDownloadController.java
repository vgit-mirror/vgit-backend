package org.venity.vgit.backend.api.controllers.repository;

import org.eclipse.jgit.api.ArchiveCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.archive.ZipFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/api/repository/download")
public class GitRepositoryDownloadController implements APIController {

    private final RepositoryDatabase repositoryDatabase;
    private final UserDatabase userDatabase;
    private final VGitConfigurationProperties properties;

    public GitRepositoryDownloadController(RepositoryDatabase repositoryDatabase, UserDatabase userDatabase, VGitConfigurationProperties properties) {
        this.repositoryDatabase = repositoryDatabase;
        this.userDatabase = userDatabase;
        this.properties = properties;
    }

    @GetMapping("/archive")
    public void archive(String repository, String branch, String token, HttpServletResponse response)
            throws APIMethodException, IOException, GitAPIException {
        RepositoryPrototype prototype = checkRepository(repositoryDatabase, userDatabase, repository, token);
        Git git = Git.open(new File(properties.getRepositoryRoot(), prototype.getPath()));

        ArchiveCommand.registerFormat("zip", new ZipFormat());
        try {
            response.setHeader("Content-Disposition", "filename=\"" + prototype.getName() + "-" + (branch != null ? branch : "master") + ".zip" + "\"");

            git.archive()
                    .setTree(git.getRepository().resolve(branch != null ? branch : "master"))
                    .setFormat("zip")
                    .setOutputStream(response.getOutputStream())
                    .call();
        } finally {
            ArchiveCommand.unregisterFormat("zip");
        }
    }
}
