package org.venity.vgit.backend.api.controllers.repository;

import org.apache.commons.io.FileUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.database.GroupDatabase;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.GroupPrototype;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.components.VGitRepositoriesHelper;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api/repository")
public class GitRepositoryController implements APIController {
    private final UserDatabase userDatabase;
    private final GroupDatabase groupDatabase;
    private final RepositoryDatabase repositoryDatabase;
    private final VGitConfigurationProperties configurationProperties;

    public GitRepositoryController(UserDatabase userDatabase, GroupDatabase groupDatabase, RepositoryDatabase repositoryDatabase, VGitConfigurationProperties configurationProperties) {
        this.userDatabase = userDatabase;
        this.groupDatabase = groupDatabase;
        this.repositoryDatabase = repositoryDatabase;
        this.configurationProperties = configurationProperties;
    }

    @RequestMapping("/create")
    public RepositoryPrototype makeRepo(String token, String name, String path, String description, String confidential)
            throws APIMethodException {

        path = VGitRepositoriesHelper.normalize(path, true);

        UserPrototype prototype = requireAuth(userDatabase, token);
        String repoPath = prototype.getLogin() + "/" + path;
        boolean repoConfidential = Boolean.parseBoolean(confidential);

        if (repoPath.equals(prototype.getLogin() + "/"))
            throw APIMethodException.ERRORS.REPOSITORY_INVALID_PATH.exception();

        if (name == null || name.isEmpty() || name.length() > 20)
            throw APIMethodException.ERRORS.REPOSITORY_INVALID_NAME.exception();

        if (confidential == null || confidential.isEmpty())
            confidential = "false";

        description = description.trim();

        repositoryDatabase.save(VGitRepositoriesHelper.create(
                configurationProperties, name, repoPath, description, prototype.getId(), repoConfidential));
        RepositoryPrototype repositoryPrototype = repositoryDatabase.findByOwnersInAndPath(prototype.getId(), repoPath);

        Set<Long> repositories = prototype.getRepositories();
        repositories.add(repositoryPrototype.getId());
        prototype.setRepositories(repositories);

        userDatabase.save(prototype);
        return repositoryPrototype;
    }

    @GetMapping("/remove")
    public Map<String, Boolean> removeRepo(String token, String path) throws APIMethodException {
        path = VGitRepositoriesHelper.normalize(path);

        UserPrototype userPrototype = requireAuth(userDatabase, token);
        RepositoryPrototype repositoryPrototype
                = repositoryDatabase.findByOwnersInAndPath(userPrototype.getId(), path);

        if (repositoryPrototype == null)
            throw APIMethodException.ERRORS.REPOSITORY_PERMISSION_DENiED.exception();

        repositoryDatabase.delete(repositoryPrototype);

        GroupPrototype groupPrototype = groupDatabase.findByName(path.split("/")[0]);
        if (groupPrototype != null) {
            Set<Long> repos = groupPrototype.getRepositories();
            repos.remove(repositoryPrototype.getId());
            groupPrototype.setRepositories(repos);
            groupDatabase.save(groupPrototype);
        }

        File root = new File(configurationProperties.getRepositoryRoot(), repositoryPrototype.getPath());

        if (!root.exists())
            return Collections.singletonMap("ok", true);

        try {
            FileUtils.deleteDirectory(root);
            return Collections.singletonMap("ok", true);
        } catch (IOException e) {
            throw APIMethodException.ERRORS.RAW_DIRECTORY_NOT_EXISTS.exception();
        }
    }
}
