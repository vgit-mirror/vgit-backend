package org.venity.vgit.backend.api.database.issues;

import org.springframework.data.jpa.repository.JpaRepository;
import org.venity.vgit.backend.api.prototypes.IssuePrototype;

public interface IssueDatabase extends JpaRepository<IssuePrototype, Long> {

}
