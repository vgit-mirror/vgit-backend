package org.venity.vgit.backend.api.controllers.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.ProfileActivityPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.components.VGitProfileActivityHelper;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api/user")
public class UserController implements APIController {
    private final UserDatabase userDatabase;
    private final VGitConfigurationProperties configurationProperties;

    public UserController(UserDatabase userDatabase, VGitConfigurationProperties configurationProperties) {
        this.userDatabase = userDatabase;
        this.configurationProperties = configurationProperties;
    }

    @GetMapping("/get/id")
    public UserPrototype getById(Long id) throws APIMethodException {
        if (id == null)
            throw APIMethodException.ERRORS.USER_GET_ID_MISMATCH.exception();

        try {
            UserPrototype prototype = userDatabase.getOne(id);

            if (prototype.isBan())
                throw APIMethodException.ERRORS.USER_GET_BANNED.exception();

            return prototype;
        } catch (EntityNotFoundException e) {
            throw APIMethodException.ERRORS.USER_GET_NOTFOUND.exception();
        }
    }

    @GetMapping("/get/login")
    public UserPrototype getByLogin(String login) throws APIMethodException {
        if (login == null)
            throw APIMethodException.ERRORS.USER_GET_ID_MISMATCH.exception();

        try {
            UserPrototype prototype = userDatabase.findByLogin(login);

            if (prototype.isBan())
                throw APIMethodException.ERRORS.USER_GET_BANNED.exception();

            return prototype;
        } catch (EntityNotFoundException e) {
            throw APIMethodException.ERRORS.USER_GET_NOTFOUND.exception();
        }
    }

    @GetMapping("/get/token")
    public UserPrototype getByToken(String token) throws APIMethodException {
        UserPrototype prototype = requireAuth(userDatabase, token);

        if (prototype.isBan())
            throw APIMethodException.ERRORS.USER_GET_BANNED.exception();

        return prototype;
    }

    @GetMapping("/get/activity/token")
    public Set<ProfileActivityPrototype.ProfileActivityElementPrototype> getActivityByToken(String token, Long startTime) throws APIMethodException {
        UserPrototype prototype = requireAuth(userDatabase, token);

        if (prototype.isBan())
            throw APIMethodException.ERRORS.USER_GET_BANNED.exception();

        return getActivitySet(VGitProfileActivityHelper.get(configurationProperties, prototype), startTime);
    }

    @GetMapping("/get/activity/id")
    public Set<ProfileActivityPrototype.ProfileActivityElementPrototype> getActivityById(Long id, Long startTime) throws APIMethodException {
        if (id == null)
            throw APIMethodException.ERRORS.USER_GET_ID_MISMATCH.exception();

        try {
            UserPrototype prototype = userDatabase.getOne(id);

            if (prototype.isBan())
                throw APIMethodException.ERRORS.USER_GET_BANNED.exception();

            return getActivitySet(VGitProfileActivityHelper.get(configurationProperties, prototype), startTime);
        } catch (EntityNotFoundException e) {
            throw APIMethodException.ERRORS.USER_GET_NOTFOUND.exception();
        }
    }

    @GetMapping("/get/activity/login")
    public Set<ProfileActivityPrototype.ProfileActivityElementPrototype> getActivityByLogin(String login, Long startTime) throws APIMethodException {
        if (login == null)
            throw APIMethodException.ERRORS.USER_GET_ID_MISMATCH.exception();

        try {
            UserPrototype prototype = userDatabase.findByLogin(login);

            if (prototype.isBan())
                throw APIMethodException.ERRORS.USER_GET_BANNED.exception();

            return getActivitySet(VGitProfileActivityHelper.get(configurationProperties, prototype), startTime);
        } catch (EntityNotFoundException e) {
            throw APIMethodException.ERRORS.USER_GET_NOTFOUND.exception();
        }
    }

    private Set<ProfileActivityPrototype.ProfileActivityElementPrototype> getActivitySet(ProfileActivityPrototype activityPrototype, Long startTime) {
        if (startTime == null)
            startTime = 0L;

        Set<ProfileActivityPrototype.ProfileActivityElementPrototype> activityElementPrototypeSet = new HashSet<>();
        Map<Integer, ProfileActivityPrototype.ProfileActivityElementPrototype> activityElementPrototypes = activityPrototype.getElements();

        for (Map.Entry<Integer, ProfileActivityPrototype.ProfileActivityElementPrototype> activityElementPrototypeEntry: activityElementPrototypes.entrySet()){
            if (activityElementPrototypeEntry.getKey() - startTime >= 0)
                activityElementPrototypeSet.add(activityElementPrototypeEntry.getValue());
            else return activityElementPrototypeSet;
        }

        return activityElementPrototypeSet;
    }

    @GetMapping("/set/status")
    public UserPrototype setStatus(String token, String status) throws APIMethodException {
        UserPrototype prototype = requireAuth(userDatabase, token);
        prototype.setStatus(status);
        userDatabase.save(prototype);
        return prototype;
    }
}
