package org.venity.vgit.backend.api.controllers.group;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.database.GroupDatabase;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.GroupPrototype;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.components.VGitRepositoriesHelper;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/api/group/repository")
public class GroupRepositoryController implements APIController {
    private final UserDatabase userDatabase;
    private final GroupDatabase groupDatabase;
    private final RepositoryDatabase repositoryDatabase;
    private final VGitConfigurationProperties configurationProperties;

    public GroupRepositoryController(UserDatabase userDatabase, GroupDatabase groupDatabase, RepositoryDatabase repositoryDatabase, VGitConfigurationProperties configurationProperties) {
        this.userDatabase = userDatabase;
        this.groupDatabase = groupDatabase;
        this.repositoryDatabase = repositoryDatabase;
        this.configurationProperties = configurationProperties;
    }

    @RequestMapping("/create")
    public RepositoryPrototype makeRepo(String token, String group, String name, String path, String description, String confidential)
            throws APIMethodException {
        if (path == null)
            throw APIMethodException.ERRORS.GROUP_NAME_MISMATCH.exception();

        path = VGitRepositoriesHelper.normalize(path, true);

        UserPrototype prototype = requireAuth(userDatabase, token);
        GroupPrototype groupPrototype = groupDatabase.findByName(group);

        if (groupPrototype == null)
            throw APIMethodException.ERRORS.GROUP_NOT_FOUND.exception();

        if (!(groupPrototype.getOwners().contains(prototype.getId())
                || groupPrototype.getMembers().contains(prototype.getId())))
            throw APIMethodException.ERRORS.GROUP_ACCESS_DENIED.exception();

        String repoPath = groupPrototype.getName() + "/" + path;
        boolean repoConfidential = Boolean.parseBoolean(confidential);

        RepositoryPrototype repositoryPrototype
                = VGitRepositoriesHelper.create(
                        configurationProperties, name, repoPath, description, prototype.getId(), repoConfidential);

        repositoryPrototype.setMembers(new HashSet<>(groupPrototype.getMembers()));
        repositoryPrototype.setOwners(new HashSet<>(groupPrototype.getOwners()));
        repositoryDatabase.save(repositoryPrototype);

        repositoryPrototype = repositoryDatabase.findByPath(repoPath);

        Set<Long> repositories = groupPrototype.getRepositories();
        repositories.add(repositoryPrototype.getId());
        groupPrototype.setRepositories(repositories);

        groupDatabase.save(groupPrototype);
        return repositoryPrototype;
    }
}
