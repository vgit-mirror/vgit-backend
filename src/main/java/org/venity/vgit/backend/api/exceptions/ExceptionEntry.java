package org.venity.vgit.backend.api.exceptions;

public class ExceptionEntry {
    public ExceptionBody error;

    public ExceptionEntry(int id, String message) {
        this.error = new ExceptionBody(id, message);
    }

    public static class ExceptionBody {
        public int code;
        public String message;

        public ExceptionBody(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }
}
