package org.venity.vgit.backend.api.controllers;

import com.fasterxml.jackson.databind.JsonMappingException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.exceptions.ExceptionEntry;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;

import java.io.IOException;

public interface APIController {

    @ExceptionHandler(APIMethodException.class)
    default ResponseEntity<ExceptionEntry> apiException(APIMethodException e) {
        return ResponseEntity.status(e.getHttpCode()).body(new ExceptionEntry(e.getCode(), e.getMessage()));
    }

    @ExceptionHandler(JsonMappingException.class)
    default ResponseEntity<ExceptionEntry> jsonMappingException(JsonMappingException e) {
        return ResponseEntity.status(500).body(new ExceptionEntry(0, "jsonMappingException"));
    }

    default RepositoryPrototype checkRepository(
            RepositoryDatabase database, UserDatabase userDatabase, String repository, String token)
            throws APIMethodException {

        if (repository == null)
            throw APIMethodException.ERRORS.REPOSITORY_GET_NOTFOUND.exception();

        if (repository.endsWith(".git"))
            repository = repository.substring(0, repository.length() - 4);

        RepositoryPrototype prototype = database.findByPath(repository);

        if (prototype == null)
            throw APIMethodException.ERRORS.REPOSITORY_GET_NOTFOUND.exception();

        if (prototype.isConfidential()) {
            UserPrototype userPrototype = requireAuth(userDatabase, token);

            if (!(prototype.getOwners().contains(userPrototype.getId())
                    || prototype.getMembers().contains(userPrototype.getId())))
                throw APIMethodException.ERRORS.AUTHORIZATION_INVALID_LOGIN.exception();
        }

        return prototype;
    }

    default UserPrototype requireAuth(UserDatabase database, String token) throws APIMethodException {
        if (token == null)
            throw APIMethodException.ERRORS.AUTHORIZATION_TOKEN_MISMATCH.exception();

        UserPrototype user = database.findByTokensIn(token);

        if (user == null)
            throw APIMethodException.ERRORS.AUTHORIZATION_INVALID_TOKEN.exception();

        return user;
    }

    @ExceptionHandler(GitAPIException.class)
    default ResponseEntity<ExceptionEntry> gitException(IOException e) {
        System.err.println(e.getMessage());
        return ResponseEntity.status(500).body(new ExceptionEntry(-1, e.getMessage()));
    }

    @ExceptionHandler(IOException.class)
    default ResponseEntity<ExceptionEntry> ioException(IOException e) {
        return ResponseEntity.status(404).body(new ExceptionEntry(-1, e.getMessage()));
    }
}
