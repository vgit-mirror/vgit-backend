package org.venity.vgit.backend.api.prototypes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.nio.charset.StandardCharsets;
import java.util.Set;

import static com.google.common.hash.Hashing.sha256;

@Entity
@Getter
@Setter
public class UserPrototype {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "_id")
    private Long id;

    @Column(name = "_registration")
    private Long registration;

    @Column(name = "_status")
    private String status;

    @Column(name="_login")
    private String login;

    @Column(name="_full_name")
    private String fullName;

    @Column(name="_email")
    private String email;

    @Column(name="_group")
    private String group;

    @ElementCollection(targetClass = Long.class)
    @JoinColumn(name = "user_repositories")
    private Set<Long> repositories;

    @Column(name="_ban")
    private boolean ban;

    // private

    @Column(name="_pass")
    @JsonIgnore
    private String password; // SHA-256

    @JoinColumn(name = "user_tokens")
    @ElementCollection(targetClass = String.class)
    @JsonIgnore
    private Set<String> tokens;

    @JoinColumn(name = "user_public_keys")
    @ElementCollection(targetClass = UserPublicKeyPrototype.class, fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<UserPublicKeyPrototype> publicKeys;

    // end private

    public UserPrototype() {

    }

    public UserPrototype(String login, String fullName, String email, String group, String password) {
        this.login = login;
        this.fullName = fullName;
        this.email = email;
        this.group = group;
        this.ban = false;
        this.registration = System.currentTimeMillis() / 1000;

        this.password = sha256().hashString(password, StandardCharsets.UTF_8).toString();
    }
}
