package org.venity.vgit.backend.api.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;

import java.util.List;

public interface RepositoryDatabase extends JpaRepository<RepositoryPrototype, Long> {
    RepositoryPrototype findByOwnersInAndPath(Long user, String path);
    RepositoryPrototype findByMembersInAndPath(Long user, String path);
    RepositoryPrototype findByPath(String path);
    List<RepositoryPrototype> findAllByMirrorNotNull();
}
