package org.venity.vgit.backend.api.controllers.repository;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand;
import org.eclipse.jgit.api.errors.CannotDeleteCurrentBranchException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.TextProgressMonitor;
import org.eclipse.jgit.revwalk.RevWalk;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import java.io.File;
import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/api/repository/branches")
public class GitRepositoryBranchesController implements APIController {

    private final UserDatabase userDatabase;
    private final RepositoryDatabase repositoryDatabase;
    private final VGitConfigurationProperties configurationProperties;

    public GitRepositoryBranchesController(UserDatabase userDatabase, RepositoryDatabase repositoryDatabase, VGitConfigurationProperties configurationProperties) {
        this.userDatabase = userDatabase;
        this.repositoryDatabase = repositoryDatabase;
        this.configurationProperties = configurationProperties;
    }

    @GetMapping("/count")
    public Map<String, Integer> count(String repository, String token)
            throws GitAPIException, IOException, APIMethodException {
        return Collections.singletonMap("count", get(repository, token).size());
    }

    @GetMapping("/default")
    public Map<String, String> getDefaultBranch(String repository, String token)
            throws APIMethodException, IOException {
        Git git = auth(token, repository, false);
        return Collections.singletonMap("branch", git.getRepository().getFullBranch());
    }

    @GetMapping("/default/set")
    public Map<String, String> setDefaultBranch(String repository, String branch, String token)
            throws APIMethodException, IOException, GitAPIException {

        if (branch == null)
            throw APIMethodException.ERRORS.BRANCH_NAME_MISMATCH.exception();

        Git git = auth(token, repository, true);

        if (!hasBranch(repository, branch, token).get("has"))
            throw APIMethodException.ERRORS.BRANCH_NOT_FOUND.exception();

        git.reset()
                .setProgressMonitor(new TextProgressMonitor())
                .setRef(branch)
                .setMode(ResetCommand.ResetType.HARD)
                .call();

        git.checkout()
                .setName(branch)
                .call();

        return getDefaultBranch(repository, token);
    }

    @GetMapping("/has")
    public Map<String, Boolean> hasBranch(String repository, String branch, String token)
            throws GitAPIException, IOException, APIMethodException {

        if (branch == null)
            throw APIMethodException.ERRORS.BRANCH_NAME_MISMATCH.exception();

        Collection<String> branches = get(repository, token);
        return Collections.singletonMap("has", branches.contains(branch));
    }

    @GetMapping("/create")
    public Collection<String> createBranch(String repository, String name, String token)
            throws GitAPIException, IOException, APIMethodException {

        if (name == null)
            throw APIMethodException.ERRORS.BRANCH_NAME_MISMATCH.exception();

        Git git = auth(token, repository, true);

        if (hasBranch(repository, name, token).get("has"))
            throw APIMethodException.ERRORS.BRANCH_CREATE_EXISTS.exception();

        RevWalk walk = new RevWalk(git.getRepository());
        git.branchCreate()
                .setName(name)
                .setStartPoint(walk.parseCommit(git.getRepository().resolve(Constants.HEAD)))
                .call();

        return get(repository, token);
    }

    @GetMapping("/remove")
    public Collection<String> removeBranch(String repository, String name, String token)
            throws GitAPIException, IOException, APIMethodException {

        if (name == null)
            throw APIMethodException.ERRORS.BRANCH_NAME_MISMATCH.exception();
        Git git = auth(token, repository, true);

        if (!hasBranch(repository, name, token).get("has"))
            throw APIMethodException.ERRORS.BRANCH_NOT_FOUND.exception();

        try {
            git.branchDelete()
                    .setBranchNames(name)
                    .call();
        } catch (CannotDeleteCurrentBranchException e) {
            throw new APIMethodException(600, e.getMessage());
        }

        return get(repository, token);
    }

    @GetMapping
    public Collection<String> get(String repository, String token)
            throws APIMethodException, IOException, GitAPIException {
        Git git = auth(token, repository, false);

        List<String> branches = new ArrayList<>();

        for (Ref branch : git.branchList().call()) {
            branches.add(branch.getName());
        }

        return branches;
    }

    private Git auth(String token, String repository, boolean force) throws APIMethodException, IOException {
        RepositoryPrototype repositoryPrototype =
                checkRepository(repositoryDatabase, userDatabase, repository, token);

        if (force || repositoryPrototype.isConfidential()) {
            UserPrototype userPrototype = requireAuth(userDatabase, token);

            if (!(repositoryPrototype.getOwners().contains(userPrototype.getId())
                    || repositoryPrototype.getMembers().contains(userPrototype.getId())))
                throw APIMethodException.ERRORS.REPOSITORY_PERMISSION_DENiED.exception();
        }

        return Git
                .open(new File(configurationProperties.getRepositoryRoot(), repositoryPrototype.getPath()));
    }
}
