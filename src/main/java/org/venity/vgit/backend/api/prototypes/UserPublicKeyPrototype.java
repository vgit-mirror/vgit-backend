package org.venity.vgit.backend.api.prototypes;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class UserPublicKeyPrototype {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @Column(length = 3000)
    private String rawKey;

    public UserPublicKeyPrototype() {

    }

    public UserPublicKeyPrototype(String name, String rawKey) {
        this.name = name;
        this.rawKey = rawKey;
    }
}
