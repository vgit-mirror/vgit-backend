package org.venity.vgit.backend.api.controllers.repository;

import org.eclipse.jgit.api.DiffCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/repository/diff")
public class GitRepositoryDiffController implements APIController {

    private final RepositoryDatabase repositoryDatabase;
    private final UserDatabase userDatabase;
    private final VGitConfigurationProperties configurationProperties;

    public GitRepositoryDiffController(RepositoryDatabase repositoryDatabase, UserDatabase userDatabase, VGitConfigurationProperties configurationProperties) {
        this.repositoryDatabase = repositoryDatabase;
        this.userDatabase = userDatabase;
        this.configurationProperties = configurationProperties;
    }

    @GetMapping
    @Async
    public String getDiff(String oldCommit, String newCommit, String repository, String token)
            throws APIMethodException, IOException, GitAPIException {
        RepositoryPrototype repositoryPrototype =
                checkRepository(repositoryDatabase, userDatabase, repository, token);

        if (repositoryPrototype.isConfidential()) {
            UserPrototype userPrototype = requireAuth(userDatabase, token);

            if (!(repositoryPrototype.getOwners().contains(userPrototype.getId())
                    || repositoryPrototype.getMembers().contains(userPrototype.getId())))
                throw APIMethodException.ERRORS.REPOSITORY_PERMISSION_DENiED.exception();
        }

        Git git = Git.open(new File(configurationProperties.getRepositoryRoot(), repositoryPrototype.getPath()));

        RevCommit commit = git.getRepository().parseCommit(git.getRepository().resolve(newCommit));

        AbstractTreeIterator newTree = prepareTreeParser(git.getRepository(), newCommit);
        AbstractTreeIterator oldTree = null;

        if (oldCommit == null) oldTree = prepareTreeParser(git.getRepository(), commit.getParent(commit.getParentCount() - 1).getName());
        else oldTree = prepareTreeParser(git.getRepository(), oldCommit);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        DiffCommand diffCommand = git.diff().setNewTree(newTree);

        if (oldTree != null)
            diffCommand.setOldTree(oldTree);

        List<DiffEntry> diff = diffCommand.call();
        for (DiffEntry entry : diff) {
            try (DiffFormatter formatter = new DiffFormatter(byteArrayOutputStream)) {
                formatter.setRepository(git.getRepository());
                formatter.format(entry);
            }
        }

        return byteArrayOutputStream.toString();
    }

    private static AbstractTreeIterator prepareTreeParser(Repository repository, String objectId) throws IOException {
        try (RevWalk walk = new RevWalk(repository)) {
            RevCommit commit = walk.parseCommit(ObjectId.fromString(objectId));
            RevTree tree = walk.parseTree(commit.getTree().getId());

            CanonicalTreeParser treeParser = new CanonicalTreeParser();
            try (ObjectReader reader = repository.newObjectReader()) {
                treeParser.reset(reader, tree.getId());
            }

            walk.dispose();

            return treeParser;
        }
    }
}
