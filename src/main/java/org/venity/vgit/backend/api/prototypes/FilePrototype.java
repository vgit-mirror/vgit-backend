package org.venity.vgit.backend.api.prototypes;

import org.eclipse.jgit.revwalk.RevCommit;

import java.io.File;

public class FilePrototype {
    public String name;
    public String path;
    public boolean isDirectory;
    public Long size;
    public CommitPrototype commit;

    public FilePrototype(String name, boolean isDirectory, RevCommit commit, Long size) {
        this.path = name;
        this.name = new File(name.replace("/", File.separator)).getName();
        this.isDirectory = isDirectory;
        this.size = size;

        if (commit != null)
            this.commit = new CommitPrototype(commit);
    }
}
