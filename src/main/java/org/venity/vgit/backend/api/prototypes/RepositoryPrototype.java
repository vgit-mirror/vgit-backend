package org.venity.vgit.backend.api.prototypes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collections;
import java.util.Set;

@Entity
@Getter
@Setter
public class RepositoryPrototype {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String path;

    private String name;

    private String description;

    @ElementCollection(targetClass = IssuePrototype.class, fetch = FetchType.LAZY)
    @Column
    @JsonIgnore
    private Set<IssuePrototype> issues;

    @ElementCollection(targetClass = Long.class, fetch = FetchType.EAGER)
    private Set<Long> owners;

    @ElementCollection(targetClass = Long.class, fetch = FetchType.EAGER)
    private Set<Long> members;

    private boolean confidential; // true - private, false - global

    private Long stars;

    private String mirror;

    public RepositoryPrototype() {

    }

    public RepositoryPrototype(String name, String path, String description, Long owner, boolean confidential) {
        this.path = path;
        this.name = name;
        this.description = description;
        this.owners = Collections.singleton(owner);
        this.members = Collections.emptySet();
        this.confidential = confidential;
        this.stars = 0L;
        this.mirror = null;
    }
}
