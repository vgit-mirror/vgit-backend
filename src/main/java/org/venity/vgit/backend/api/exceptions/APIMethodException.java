package org.venity.vgit.backend.api.exceptions;

import lombok.Getter;

@Getter
public class APIMethodException extends Exception {

    @Getter
    public enum ERRORS {
        REGISTRATION_LOGIN_MISMATCH(101, "Login mismatch!"),
        REGISTRATION_PASSWORD_MISMATCH(102, "Password mismatch!"),
        REGISTRATION_EMAIL_MISMATCH(103, "Email mismatch!"),
        REGISTRATION_NAME_MISMATCH(104, "Full Name mismatch!"),
        REGISTRATION_LOGIN_EXIST(105, "User with this login already exists!"),
        REGISTRATION_EMAIL_EXIST(106, "User with this email already exists!"),
        REGISTRATION_INVALID_LOGIN(107, "Invalid login!"),
        REGISTRATION_INVALID_EMAIL(108, "Invalid email!"),
        REGISTRATION_INVALID_PASSWORD(109, "Invalid password!"),

        AUTHORIZATION_LOGIN_MISMATCH(201, "Login mismatch!"),
        AUTHORIZATION_PASSWORD_MISMATCH(202, "Password mismatch!"),
        AUTHORIZATION_TOKEN_MISMATCH(202, "Token mismatch!"),
        AUTHORIZATION_INVALID_LOGIN(203, "Invalid login!"),
        AUTHORIZATION_INVALID_PASSWORD(204, "Invalid password!"),
        AUTHORIZATION_INVALID_TOKEN(205, "Invalid token!"),

        REPOSITORY_CREATE_EXISTS(301, "Repository already exists!"),
        REPOSITORY_CREATE_ERROR(302, "Unknown error!"),
        REPOSITORY_GET_ID_MISMATCH(303, "Id mismatch!"),
        REPOSITORY_GET_NOTFOUND(304, "Repository not found!"),
        REPOSITORY_PERMISSION_DENiED(305, 403, "Repository permission denied!"),
        REPOSITORY_INVALID_PATH(306, "Invalid path!"),
        REPOSITORY_INVALID_NAME(307, "Invalid name!"),

        USER_GET_ID_MISMATCH(401, "Id mismatch!"),
        USER_GET_LOGIN_MISMATCH(402, "Login mismatch!"),
        USER_GET_NOTFOUND(403, "User not found!"),
        USER_GET_BANNED(404, "User baned!"),

        RAW_FILE_NOT_EXISTS(501, "Raw file not!"),
        RAW_DIRECTORY_NOT_EXISTS(502, "Raw directory not!"),

        BRANCH_NAME_MISMATCH(601, "Name mismatch!"),
        BRANCH_CREATE_EXISTS(602, "Branch already exists!"),
        BRANCH_NOT_FOUND(603, "Branch not found!"),

        COMMIT_NOT_FOUND(701, "Commit not found!"),

        ISSUE_CREATE_NAME_MISMATCH(801, "Name mismatch!"),
        ISSUE_CREATE_CONTENT_MISMATCH(802, "Content mismatch!"),
        ISSUE_GET_ID_MISMATCH(803, "Id mismatch!"),
        ISSUE_GET_NOT_FOUND(804, "Issue not found!"),
        ISSUE_ACCESS_DENIED(805, "Issue access denied!"),

        GROUP_NOT_FOUND(901, "Group not found!"),
        GROUP_ACCESS_DENIED(902, "Group access denied!"),
        GROUP_EXISTS(903, "Group already exists!"),
        GROUP_NAME_MISMATCH(904, "Group name mismatch!"),
        ;

        int code;
        int httpCode;
        String message;

        ERRORS(int code, int httpCode, String message) {
            this.code = code;
            this.httpCode = httpCode;
            this.message = message;
        }

        ERRORS(int code, String message) {
            this.code = code;
            this.httpCode = 400;
            this.message = message;
        }

        public APIMethodException exception() {
            return new APIMethodException(this);
        }

        public ExceptionEntry entry() {
            return new ExceptionEntry(code, message);
        }
    }

    private int code;
    private int httpCode;

    public APIMethodException(int code, String message) {
        super(message);
        this.code = code;
        this.httpCode = 400;
    }

    public APIMethodException(int code, int httpCode, String message) {
        super(message);
        this.code = code;
        this.httpCode = httpCode;
    }

    public APIMethodException(ERRORS error) {
        super(error.getMessage());
        this.code = error.getCode();
        this.httpCode = error.getHttpCode();
    }
}
