package org.venity.vgit.backend.api.prototypes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class IssuePrototype {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "_id")
    private Long id;

    @Column(name = "_name")
    private String name;

    @Column(name = "_isOpen")
    private boolean isOpen;

    @ElementCollection(targetClass = IssueLabelPrototype.class)
    @Column(name = "_labels")
    private Set<IssueLabelPrototype> labels;

    @JsonIgnore
    @ElementCollection(targetClass = IssueCommentPrototype.class)
    @Column(name = "_comments")
    private Set<IssueCommentPrototype> comments;

    public IssuePrototype() {
    }

    public IssuePrototype(String name, IssueCommentPrototype prototype) {
        this.name = name;
        this.comments = Collections.singleton(prototype);
        this.isOpen = true;
        this.labels = new HashSet<>();
    }

    @Entity
    @Getter
    @Setter
    public static class IssueCommentPrototype {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "_id")
        public Long id;
        public Long creator;
        public String content;
        public Long time;

        public IssueCommentPrototype() {
        }

        public IssueCommentPrototype(Long creator, String content) {
            this.creator = creator;
            this.content = content;
            this.time = System.currentTimeMillis() / 1000;
        }

    }

    @Entity
    @Setter
    @Getter
    public static class IssueLabelPrototype {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "_id")
        public Long id;
        public String name;
        public String color;

        public IssueLabelPrototype(String name, String color) {
            this.name = name;
            this.color = color;
        }

        public IssueLabelPrototype() {
        }
    }
}
