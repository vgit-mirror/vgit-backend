package org.venity.vgit.backend.api.controllers.repository;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.CommitPrototype;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/repository/commits")
public class GitRepositoryCommitsController implements APIController {
    private final UserDatabase userDatabase;
    private final RepositoryDatabase repositoryDatabase;
    private final VGitConfigurationProperties configurationProperties;

    public GitRepositoryCommitsController(UserDatabase userDatabase, RepositoryDatabase repositoryDatabase, VGitConfigurationProperties configurationProperties) {
        this.userDatabase = userDatabase;
        this.repositoryDatabase = repositoryDatabase;
        this.configurationProperties = configurationProperties;
    }

    @Async
    @GetMapping("/count")
    public Map<String, Long> count(String repository, String branch, String token)
            throws GitAPIException, IOException, APIMethodException {

        RepositoryPrototype prototype = checkRepository(repositoryDatabase, userDatabase, repository, token);
        Long i = 0L;
        Git git = Git
                .open(new File(configurationProperties.getRepositoryRoot(), prototype.getPath()));

        List<Ref> branches = git.branchList().call();

        for (Ref gbranch : branches) {
            String branchName = gbranch.getName();

            if (!branchName.equals(branch))
                continue;

            Iterable<RevCommit> commits = git.log().all().call();

            for (RevCommit commit : commits) {
                for (Map.Entry<String, Ref> e : git.getRepository().getAllRefs().entrySet()) {
                    if (e.getKey().startsWith(Constants.R_HEADS)) {
                        String foundInBranch = e.getValue().getName();
                        if (branchName.equals(foundInBranch)) {
                            i++;
                            break;
                        }
                    }
                }
            }
        }

        return Collections.singletonMap("count", i);
    }

    @Async
    @GetMapping
    public List<CommitPrototype> getCommits(String repository, String branch, Integer offset, Integer count, String token)
            throws APIMethodException, IOException, GitAPIException {
        if (repository == null)
            throw APIMethodException.ERRORS.REPOSITORY_GET_NOTFOUND.exception();

        if (branch == null)
            throw APIMethodException.ERRORS.REPOSITORY_GET_NOTFOUND.exception(); // TODO: make normal errors

        if (count == null)
            count = 0;

        if (offset == null)
            offset = 0;

        RepositoryPrototype prototype = checkRepository(repositoryDatabase, userDatabase, repository, token);
        List<CommitPrototype> prototypes = new ArrayList<>();
        Git git = Git
                .open(new File(configurationProperties.getRepositoryRoot(), prototype.getPath()));

        int c = -1;
        List<Ref> branches = git.branchList().call();
        for (Ref gbranch : branches) {
            String branchName = gbranch.getName();

            if (!branchName.equals(branch))
                continue;


            Iterable<RevCommit> commits = git.log().all().setMaxCount(count + offset).call();

            for (RevCommit commit : commits) {
                for (Map.Entry<String, Ref> e : git.getRepository().getAllRefs().entrySet()) {
                    if (e.getKey().startsWith(Constants.R_HEADS)) {
                        String foundInBranch = e.getValue().getName();
                        if (branchName.equals(foundInBranch)) {
                            c++;

                            if (c > count + offset) return prototypes;
                            if (c >= offset) {
                                if (commit.getParentCount() > 0) {
                                    prototypes.add(new CommitPrototype(commit, commit.getParents()[0].getName()));
                                } else {
                                    prototypes.add(new CommitPrototype(commit));
                                }
                            }


                            break;
                        }
                    }
                }
            }
        }

        return prototypes;
    }
}
