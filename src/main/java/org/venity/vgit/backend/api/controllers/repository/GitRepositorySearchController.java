package org.venity.vgit.backend.api.controllers.repository;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/repository/search")
public class GitRepositorySearchController implements APIController {

    private final RepositoryDatabase repositoryDatabase;
    private final UserDatabase userDatabase;

    public GitRepositorySearchController(RepositoryDatabase repositoryDatabase, UserDatabase userDatabase) {
        this.repositoryDatabase = repositoryDatabase;
        this.userDatabase = userDatabase;
    }

    @GetMapping("/id")
    public RepositoryPrototype getById(Long id, String token) throws APIMethodException {
        if (id == null)
            throw APIMethodException.ERRORS.REPOSITORY_GET_ID_MISMATCH.exception();

        try {
            RepositoryPrototype prototype = repositoryDatabase.getOne(id);

            if (prototype.isConfidential()) {
                UserPrototype userPrototype = requireAuth(userDatabase, token);

                if (!(prototype.getOwners().contains(userPrototype.getId())
                        || prototype.getMembers().contains(userPrototype.getId()))) {
                    throw APIMethodException.ERRORS.REPOSITORY_PERMISSION_DENiED.exception();
                }
            }

            return prototype;
        } catch (EntityNotFoundException e) {
            throw APIMethodException.ERRORS.REPOSITORY_GET_NOTFOUND.exception();
        }
    }

    @GetMapping("/ids")
    public Collection<RepositoryPrototype> getByIds(String ids, String token) throws APIMethodException {
        if (ids == null)
            throw APIMethodException.ERRORS.REPOSITORY_GET_ID_MISMATCH.exception();

        try {
            List<RepositoryPrototype> prototypes = new ArrayList<>();

            for (String id : ids.split(",")) {
                prototypes.add(getById(Long.parseLong(id), token));
            }

            return prototypes;
        } catch (Throwable e) {
            if (e instanceof APIMethodException)
                throw e;
            else throw APIMethodException.ERRORS.REPOSITORY_GET_NOTFOUND.exception();
        }
    }

    @GetMapping
    public RepositoryPrototype getByPath(String path, String token) throws APIMethodException {
        if (path == null)
            throw APIMethodException.ERRORS.REPOSITORY_GET_ID_MISMATCH.exception();

        RepositoryPrototype prototype = repositoryDatabase.findByPath(path);

        if (prototype == null)
            throw APIMethodException.ERRORS.REPOSITORY_GET_NOTFOUND.exception();

        if (prototype.isConfidential()) {
            UserPrototype userPrototype = requireAuth(userDatabase, token);

            if (!(prototype.getOwners().contains(userPrototype.getId())
                    || prototype.getMembers().contains(userPrototype.getId()))) {
                throw APIMethodException.ERRORS.REPOSITORY_PERMISSION_DENiED.exception();
            }
        }

        return prototype;
    }
}
