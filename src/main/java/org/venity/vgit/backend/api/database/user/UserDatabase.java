package org.venity.vgit.backend.api.database.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.venity.vgit.backend.api.prototypes.UserPrototype;

public interface UserDatabase extends JpaRepository<UserPrototype, Long> {
    UserPrototype findByTokensIn(String token);
    UserPrototype findByEmail(String email);
    UserPrototype findByLogin(String login);
}
