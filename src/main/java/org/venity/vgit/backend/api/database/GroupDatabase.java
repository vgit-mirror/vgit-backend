package org.venity.vgit.backend.api.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.venity.vgit.backend.api.prototypes.GroupPrototype;

import java.util.Optional;

public interface GroupDatabase extends JpaRepository<GroupPrototype, Long> {
    GroupPrototype findByName(String name);
    Optional<GroupPrototype> findOptByName(String name);
    GroupPrototype findByNameAndOwnersIn(String name, Long owner);
}
