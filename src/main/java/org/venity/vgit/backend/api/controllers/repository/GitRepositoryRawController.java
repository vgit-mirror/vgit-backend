package org.venity.vgit.backend.api.controllers.repository;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.exceptions.ExceptionEntry;
import org.venity.vgit.backend.api.prototypes.FilePrototype;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.configuratons.VGitConfigurationProperties;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/api/repository/raw")
public class GitRepositoryRawController implements APIController {

    private final UserDatabase userDatabase;
    private final RepositoryDatabase repositoryDatabase;
    private final VGitConfigurationProperties configurationProperties;

    public GitRepositoryRawController(UserDatabase userDatabase, RepositoryDatabase repositoryDatabase, VGitConfigurationProperties configurationProperties) {
        this.userDatabase = userDatabase;
        this.repositoryDatabase = repositoryDatabase;
        this.configurationProperties = configurationProperties;
    }

    @GetMapping("/tree")
    public Collection<FilePrototype> getTree(String repository, String path, String token, String lastCommit) throws APIMethodException, IOException, GitAPIException {

        if (path.endsWith("/") && !path.equals("/"))
            path = path.substring(0, path.length() - 1);

        Git git = getGitByPath(repository, token);
        Repository repo = git.getRepository();
        List<FilePrototype> prototypes = new ArrayList<>();

        ObjectId lastCommitId = repo.resolve(lastCommit != null ? lastCommit : Constants.HEAD);

        if (lastCommitId == null)
            return prototypes;

        RevWalk revWalk = new RevWalk(repo);
        RevCommit commit = revWalk.parseCommit(lastCommitId);

        if (commit == null)
            return prototypes;

        RevTree tree = commit.getTree();
        TreeWalk treeWalk = new TreeWalk(repo);
        treeWalk.setRecursive(false);

        int id = treeWalk.addTree(tree);

        while (treeWalk.next()) {

            String name = "/" + treeWalk.getPathString();
            if (new File(name).getParent().replace("\\", "/").equals(path)) {
                ObjectId objectId = treeWalk.getObjectId(id);
                ObjectLoader loader = repo.open(objectId);

                Iterable<RevCommit> log = git.log()
                        .addPath(treeWalk.getPathString())
                        .add(commit)
                        .setMaxCount(1).call();

                prototypes.add(new FilePrototype(name, treeWalk.isSubtree(), log.iterator().next(), loader.getSize()));
            }

            if (treeWalk.isSubtree())
                treeWalk.enterSubtree();
        }

        prototypes.sort(Comparator.comparingInt(filePrototype -> (filePrototype.isDirectory ? 0 : 1)));

        return prototypes;
    }

    @GetMapping("/isDirectory")
    public Map<String, Boolean> isDirectory(String repository, String path, String token, String lastCommit) throws APIMethodException, IOException {
        try {
            return Collections.singletonMap("isDirectory", getSingletonTreeWalkByPath(repository, path, token, lastCommit).isSubtree());
        } catch (NullPointerException e) {
            throw APIMethodException.ERRORS.RAW_FILE_NOT_EXISTS.exception();
        }
    }

    @GetMapping("/file")
    public void getFile(String repository, String path, String token, String lastCommit, HttpServletResponse response) throws APIMethodException, IOException {
        Repository repo = getGitByPath(repository, token).getRepository();

        try {
            repo.open(getSingletonTreeWalkByPath(repository, path, token, lastCommit).getObjectId(0)).copyTo(response.getOutputStream());
        } catch (Throwable e) {
            throw APIMethodException.ERRORS.RAW_FILE_NOT_EXISTS.exception();
        }
    }

    public TreeWalk getSingletonTreeWalkByPath(String repository, String path, String token, String lastCommit) throws IOException, APIMethodException {
        Repository repo = getGitByPath(repository, token).getRepository();

        if (path.startsWith("/"))
            path = path.substring(1);

        ObjectId lastCommitId = repo.resolve(lastCommit != null ? lastCommit : Constants.HEAD);

        if (lastCommitId == null)
            throw APIMethodException.ERRORS.RAW_FILE_NOT_EXISTS.exception();

        RevWalk revWalk = new RevWalk(repo);
        RevCommit commit = revWalk.parseCommit(lastCommitId);

        if (commit == null)
            throw APIMethodException.ERRORS.RAW_FILE_NOT_EXISTS.exception();

        RevTree tree = commit.getTree();
        return TreeWalk.forPath(repo, path, tree);
    }

    public Git getGitByPath(String repository, String token) throws IOException, APIMethodException {
        RepositoryPrototype repositoryPrototype = checkRepository(repositoryDatabase, userDatabase, repository, token);
        return Git.open(new File(configurationProperties.getRepositoryRoot(), repositoryPrototype.getPath()));
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<ExceptionEntry> ioException(IOException e) {
        System.err.println(e.getMessage());
        return ResponseEntity.status(400).body(APIMethodException.ERRORS.RAW_FILE_NOT_EXISTS.entry());
    }
}
