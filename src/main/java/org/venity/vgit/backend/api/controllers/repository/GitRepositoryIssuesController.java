package org.venity.vgit.backend.api.controllers.repository;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.venity.vgit.backend.api.controllers.APIController;
import org.venity.vgit.backend.api.database.RepositoryDatabase;
import org.venity.vgit.backend.api.database.user.UserDatabase;
import org.venity.vgit.backend.api.database.issues.IssueCommentDatabase;
import org.venity.vgit.backend.api.database.issues.IssueDatabase;
import org.venity.vgit.backend.api.database.issues.IssueLabelDatabase;
import org.venity.vgit.backend.api.exceptions.APIMethodException;
import org.venity.vgit.backend.api.prototypes.IssuePrototype;
import org.venity.vgit.backend.api.prototypes.RepositoryPrototype;
import org.venity.vgit.backend.api.prototypes.UserPrototype;

import javax.persistence.EntityNotFoundException;
import java.util.*;

@RestController
@RequestMapping("/api/repository/issues")
public class GitRepositoryIssuesController implements APIController {
    private final UserDatabase userDatabase;
    private final RepositoryDatabase repositoryDatabase;
    private final IssueDatabase issueDatabase;
    private final IssueCommentDatabase issueCommentDatabase;
    private final IssueLabelDatabase issueLabelDatabase;

    public GitRepositoryIssuesController(UserDatabase userDatabase, RepositoryDatabase repositoryDatabase, IssueDatabase issueDatabase, IssueCommentDatabase issueCommentDatabase, IssueLabelDatabase issueLabelDatabase) {
        this.userDatabase = userDatabase;
        this.repositoryDatabase = repositoryDatabase;
        this.issueDatabase = issueDatabase;
        this.issueCommentDatabase = issueCommentDatabase;
        this.issueLabelDatabase = issueLabelDatabase;
    }

    @GetMapping("/create")
    public IssuePrototype createNewIssue(String name, String content, String repository, String token) throws APIMethodException {
        UserPrototype user = requireAuth(userDatabase, token);
        RepositoryPrototype repositoryPrototype = checkRepository(repositoryDatabase, userDatabase, repository, token);

        if (name == null || name.isEmpty())
            throw APIMethodException.ERRORS.ISSUE_CREATE_NAME_MISMATCH.exception();

        if (content == null || content.isEmpty())
            throw APIMethodException.ERRORS.ISSUE_CREATE_CONTENT_MISMATCH.exception();


        IssuePrototype issuePrototype = createNewIssuePrototype(name, createNewIssueCommentPrototype(user.getId(), content));
        Set<IssuePrototype> issues = repositoryPrototype.getIssues();
        if (issues == null){
            issues = new HashSet<>();
        }
        issues.add(issuePrototype);
        repositoryPrototype.setIssues(issues);
        repositoryDatabase.save(repositoryPrototype);
        return issuePrototype;
    }

    @GetMapping("/count")
    public Map<String, Integer> getIssuesCount(String repository, String token) throws APIMethodException {
        RepositoryPrototype repositoryPrototype = checkRepository(repositoryDatabase, userDatabase, repository, token);

        try {
            return Collections.singletonMap("count", repositoryPrototype.getIssues().size());
        } catch (NullPointerException e) {
            return Collections.singletonMap("count", 0);
        }
    }

    @GetMapping("/close")
    public Map<String, Boolean> closeIssue(String repository, Long id, String token) throws APIMethodException {
        RepositoryPrototype repositoryPrototype = checkRepository(repositoryDatabase, userDatabase, repository, token);
        UserPrototype userPrototype   = requireAuth(userDatabase, token);
        IssuePrototype issuePrototype = getIssueFromRepository(id, repository, token);
        IssuePrototype.IssueCommentPrototype firstIssueComment = issuePrototype.getComments().iterator().next();

        if (!repositoryPrototype.getIssues().contains(issuePrototype))
            throw APIMethodException.ERRORS.ISSUE_ACCESS_DENIED.exception();

        if (firstIssueComment.getCreator().equals(userPrototype.getId())
                || repositoryPrototype.getOwners().contains(userPrototype.getId())
                || repositoryPrototype.getMembers().contains(userPrototype.getId())) {
            if (issuePrototype.isOpen()) {
                issuePrototype.setOpen(false);
                issueDatabase.save(issuePrototype);
                return Collections.singletonMap("ok", true);
            }
        }

        return Collections.singletonMap("ok", false);
    }

    @GetMapping("/open")
    public Map<String, Boolean> openIssue(String repository, Long id, String token) throws APIMethodException {
        RepositoryPrototype repositoryPrototype = checkRepository(repositoryDatabase, userDatabase, repository, token);
        UserPrototype userPrototype   = requireAuth(userDatabase, token);
        IssuePrototype issuePrototype = getIssueFromRepository(id, repository, token);
        IssuePrototype.IssueCommentPrototype firstIssueComment = issuePrototype.getComments().iterator().next();

        if (!repositoryPrototype.getIssues().contains(issuePrototype))
            throw APIMethodException.ERRORS.ISSUE_ACCESS_DENIED.exception();

        if (firstIssueComment.getCreator().equals(userPrototype.getId())
                || repositoryPrototype.getOwners().contains(userPrototype.getId())
                || repositoryPrototype.getMembers().contains(userPrototype.getId())) {
            if (!issuePrototype.isOpen()) {
                issuePrototype.setOpen(true);
                issueDatabase.save(issuePrototype);
                return Collections.singletonMap("ok", true);
            }
        }

        return Collections.singletonMap("ok", false);
    }

    @GetMapping
    public Collection<IssuePrototype> getIssues(String repository, Integer count, Integer offset, String token)
            throws APIMethodException {
        RepositoryPrototype repositoryPrototype = checkRepository(repositoryDatabase, userDatabase, repository, token);

        if (count == null)
            count = 0;

        if (offset == null)
            offset = 0;

        List<IssuePrototype> issues = new ArrayList<>(repositoryPrototype.getIssues());

        if (count == 0)
            return issues;

        if (issues.size() >= offset + count) {
            return issues.subList(offset, offset + count);
        } else {
            return issues.subList(offset, issues.size());
        }
    }

    @GetMapping("/comments/count")
    public Map<String, Integer> getIssueCommentsCount(Long id, String repository, String token, int count, int offset)
            throws APIMethodException {
        IssuePrototype prototype = getIssueFromRepository(id, repository, token);

        try {
            return Collections.singletonMap("count", prototype.getComments().size());
        } catch (NullPointerException e) {
            return Collections.singletonMap("count", 0);
        }
    }

    @GetMapping("/comments")
    public Collection<IssuePrototype.IssueCommentPrototype> getIssuesComments(
            Long id, String repository, String token, Integer count, Integer offset)
            throws APIMethodException {
        IssuePrototype prototype = getIssueFromRepository(id, repository, token);
        List<IssuePrototype.IssueCommentPrototype> comments = new ArrayList<>(prototype.getComments());

        if (count == null)
            count = 0;

        if (offset == null)
            offset = 0;

        if (count == 0)
            return comments;

        if (comments.size() >= offset + count)
            return comments.subList(offset, offset + count);
        else return comments.subList(offset, comments.size());
    }

    @GetMapping("/comments/add")
    public IssuePrototype.IssueCommentPrototype addIssueComment(
            Long id, String content, String repository, String token)
            throws APIMethodException {
        UserPrototype userPrototype = requireAuth(userDatabase, token);
        IssuePrototype prototype = getIssueFromRepository(id, repository, token);
        Set<IssuePrototype.IssueCommentPrototype> comments = prototype.getComments();
        IssuePrototype.IssueCommentPrototype commentPrototype = createNewIssueCommentPrototype(userPrototype.getId(), content);
        comments.add(commentPrototype);
        prototype.setComments(comments);
        issueDatabase.save(prototype);
        return commentPrototype;
    }

    private IssuePrototype getIssueFromRepository(Long id, String repository, String token) throws APIMethodException {
        RepositoryPrototype repositoryPrototype = checkRepository(repositoryDatabase, userDatabase, repository, token);

        if (id == null)
            throw APIMethodException.ERRORS.ISSUE_GET_ID_MISMATCH.exception();

        IssuePrototype issuePrototype = null;

        try {
            issuePrototype = issueDatabase.getOne(id);

            if (!repositoryPrototype.getIssues().contains(issuePrototype))
                throw new EntityNotFoundException();
        } catch (EntityNotFoundException e) {
            throw APIMethodException.ERRORS.ISSUE_GET_NOT_FOUND.exception();
        }

        return issuePrototype;
    }

    private IssuePrototype createNewIssuePrototype(String name, IssuePrototype.IssueCommentPrototype content){
        IssuePrototype prototype = new IssuePrototype(name, content);
        issueDatabase.save(prototype);
        return prototype;
    }

    private IssuePrototype.IssueCommentPrototype createNewIssueCommentPrototype(Long id, String content){
        IssuePrototype.IssueCommentPrototype prototype = new IssuePrototype.IssueCommentPrototype(id, content);
        issueCommentDatabase.save(prototype);
        return prototype;
    }

    private IssuePrototype.IssueLabelPrototype createNewIssueLabelPrototype(String name, String color){
        IssuePrototype.IssueLabelPrototype prototype = new IssuePrototype.IssueLabelPrototype(name, color);
        issueLabelDatabase.save(prototype);
        return prototype;
    }
}
