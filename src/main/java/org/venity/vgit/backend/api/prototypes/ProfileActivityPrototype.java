package org.venity.vgit.backend.api.prototypes;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

@Getter
@Setter
public class ProfileActivityPrototype implements Serializable {
    private Map<Integer, ProfileActivityElementPrototype> elements;
    private Long profileId;

    public ProfileActivityPrototype(Long profileId, Map<Integer, ProfileActivityElementPrototype> elements) {
        this.elements = elements;
        this.profileId = profileId;
    }

    @Getter
    @Setter
    public static class ProfileActivityElementPrototype implements Serializable {
        private String type;
        private Map<String, String> data;

        public ProfileActivityElementPrototype(String type, Map<String, String> data) {
            this.type = type;
            this.data = data;
        }
    }
}
